import csv
import numpy as np
import scipy as sp
#lenstool_train=csv.reader(open("../lenstool/lenstooltraining.csv","wb"))
lenstool_train=np.genfromtxt("../lenstool/lenstooltraining.csv", delimiter=',',dtype=float)
print lenstool_train[:2]
ground_truth=np.genfromtxt("../Raw/Training_halos.csv", delimiter=",",skiprows=1,usecols=(4,5),dtype=float)
print ground_truth[:2]

ground_truth=ground_truth[:99]
print len(ground_truth),len(lenstool_train)
bias=(ground_truth-lenstool_train).mean(axis=0)
print bias
#print lenstool_train-bias

lenstool_benchmark=np.genfromtxt("../misc/lenstool.benchmark.csv",delimiter=",",skiprows=1,usecols=(1,2,3,4,5,6))
print len(lenstool_benchmark)
lens2=[]
print lenstool_benchmark[:2]
for i in range(0,41):
	lens2.append(list(lenstool_benchmark[i][:2]+bias)+[0,0,0,0])
print lens2[:2]
for i in range(41,120):
	#print i
	lens2.append(list(lenstool_benchmark[i]))

with open("bias.csv","wb") as f:
	writer=csv.writer(f)

	for i in range(120):
		writer.writerow(["Sky"+str(i+1)]+list(lens2[i]))
