import csv
import pylab as p
from matplotlib.backends.backend_pdf import PdfPages
import math
from numpy import linspace
from scipy import pi,sin,cos

def getforce(mass,r):
	return mass/r

def einasto(mass,r,A,alpha):
	return mass*math.exp(-A*r**alpha)

def distance(x1,y1,x2,y2):
	return math.sqrt((x2-x1)**2 + (y2-y1)**2)

def visualize(halo,galaxies,color):
	scale=50

	fig = p.figure(figsize=(8,8))
	p.axis([0,4200,0,4200])
	p.grid(True)
	for hal in halo:
		#print hal
		X,Y=ellipse(200,200,3.1416/2,float(hal[0]),float(hal[1]))
		p.plot(X,Y,"c.-",ms=2) # cyan
	for galaxy in galaxies:
		x=float(galaxy[1])		
		y=float(galaxy[2])
		e1=float(galaxy[3])
		e2=float(galaxy[4])

		e=math.sqrt(e1**2+e2**2)
		a = 1/(1.-e)*scale
		b=1/(1.+e)*scale
		theta=math.atan2(e2,e1)/2.
		#print theta
		X,Y=ellipse(a,b,theta,x,y,Nb=20)
		p.plot(X,Y,color+".-",ms=1) # black el
	p.savefig(pp, format='pdf')
	p.clf()


def ellipse(ra,rb,ang,x0,y0,Nb=20):
	'''ra - major axis length
 rb - minor axis length
 ang - angle
 x0,y0 - position of centre of ellipse
 Nb - No. of points that make an ellipse
'''
	xpos,ypos=x0,y0
	radm,radn=ra,rb
	an=ang

	co,si=cos(an),sin(an)
	the=linspace(0,2*pi,Nb)
	X=radm*cos(the)*co-si*radn*sin(the)+xpos
	Y=radm*cos(the)*si+co*radn*sin(the)+ypos
	return X,Y

#Read halos data
pp = PdfPages('../plots/2telta.pdf')

halos=[]
#reader=csv.reader(open("../lenstool/Results/2trlt2.csv","rb"))
reader=csv.reader(open("../lenstool/Results/2telt.csv","rb"))
#reader=csv.reader(open("../ellipse/Results/2trpo1.csv","rb"))
#reader=csv.reader(open("../Raw/Training_halos.csv","rb"))
reader.next()
for i in reader:
	halos.append(i)
#Read skies
for i in range(41,81):
	print i
	reader=csv.reader(open("../Raw/Test_Skies/Test_Sky"+str(i)+".csv","rb"))
	#reader=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(i)+".csv","rb"))
	reader.next()
	sky=[]
	for j in reader:
		sky.append(j)
	#print sky[0]
	#print halos[0]
	#halo=[halos[i][4:6],halos[i][6:8]]
	halo=[halos[i-41][1:3],halos[i-41][3:5]]
	print halos[i-41]
	print halo
	newsky=[]
	for k in sky:
		xgalaxy=float(k[1])
		ygalaxy=float(k[2])
		e1,e2=float(k[3]),float(k[4])
		xhalo=float(halo[0][0])
		yhalo=float(halo[0][1])
		r = distance(xhalo, yhalo, xgalaxy, ygalaxy)
		phi = math.atan2(ygalaxy - yhalo, xgalaxy - xhalo)
		#force = getforce(mass=1, r)
		force=einasto(mass=.8,r=r,A=.018,alpha=.6) #milestone1
		#force=einasto(mass=.22,r=r,A=.009,alpha=.4)
		
		e1 += force*cos(2*phi)
		e2 += force*sin(2*phi)
		newsky.append([k[0],float(k[1]),float(k[2]),e1,e2])

	#with open("processed/Train_Skies/Training_Sky"+str(i)+".csv","wb") as f:
	with open("milestone/processed/Test_Skies/Test_Sky"+str(i)+".csv","wb") as f:
		writer=csv.writer(f)
		writer.writerow("GalaxyID,x,y,e1,e2".split(","))
		for galaxy in newsky:
			writer.writerow(galaxy)
		visualize(halo,sky,color="k")
		visualize(halo,newsky,color="y")
#print halos

#p.hold(False) #Fix memory leak in matplotlib

#visualize(halo,sky)


	#

#print newsky[0]
pp.close()

			