
import subprocess
import csv
import numpy as np
from scipy.optimize import minimize 
import math
from scipy import pi,sin,cos
from time import sleep
def einasto(mass,r,A,alpha):
	return mass*math.exp(-A*r**alpha)

def distance(x1,y1,x2,y2):
	return math.sqrt((x2-x1)**2 + (y2-y1)**2)

def processSky(sky,mass,A,alpha):
	newsky=[]
	for k in sky:
		xgalaxy=float(k[1])
		ygalaxy=float(k[2])
		e1,e2=float(k[3]),float(k[4])
		xhalo=float(halo[0][0])
		yhalo=float(halo[0][1])
		r = distance(xhalo, yhalo, xgalaxy, ygalaxy)
		phi = math.atan2(ygalaxy - yhalo, xgalaxy - xhalo)
		#force = getforce(mass=1, r)
		force=einasto(mass=mass,r=r,A=A,alpha=alpha)
		e1 += force*cos(2*phi)
		e2 += force*sin(2*phi)
		newsky.append([k[0],float(k[1]),float(k[2]),e1,e2])
	return newsky
def runlens(argss,sky,halo):
	print argss
	mass,A,alpha=argss[0],argss[1],argss[2]
	newsky=processSky(sky,mass,A,alpha)
	print newsky[1]
	
	with open("Training_Sky_a101.csv","wb") as f:
		writer=csv.writer(f)
		writer.writerow(["header"])
		writer.writerows(newsky)
	#subprocess.call(["python","../lenstool/preprocesslt.py"])
	sleep(1)
	subprocess.call(["python","../maximumLikelihood/Maximum_likelihood_Benchmark.py"])
	
	#subprocess.Popen(["python","../lenstool/runlens1halo.py"], \
	#	stderr=subprocess.STDOUT, stdout=subprocess.PIPE).communicate()[0]
	with open("mltemp.csv","rb") as f:
		reader=csv.reader(f)
		reader.next()
		mlres=[]
		for i in reader:
			mlres.append(i)
			#ltres=reader.next()
	print halo
	print mlres[100]
	ltres=mlres[100]
	dist=distance(float(halo[1][0]),float(ltres[1]),float(halo[1][1]),float(ltres[2]))
	print "dist",dist
	return dist
reader=csv.reader(open("../Raw/Training_halos.csv","rb"))
#reader.next()
halos=[]
for i in reader:
	halos.append(i)
ress=[]
for i in range(101,201):
	print i
	#reader=csv.reader(open("../Raw/Test_Skies/Test_Sky"+str(i)+".csv","rb"))
	reader=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(i)+".csv","rb"))
	reader.next()
	sky=[]
	for j in reader:
		sky.append(j)
	#print sky[0]
	#print halos[0]
	halo=[halos[i][4:6],halos[i][6:8]]
	#halo=[halos[i-41][1:3],halos[i-41][3:5]]
	#print halos[i]
	#print halo
	x0 = [1,.02,.5]
	res=minimize(runlens,x0, method='nelder-mead',args=(sky,halo),options={"maxiter":10})
	print res
	print res.x
	with open("ress"+str(i)+".csv","wb") as f:
		writer=csv.writer(f)
		writer.writerow([res]+res.x)
