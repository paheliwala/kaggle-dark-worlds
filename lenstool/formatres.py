import csv

inp=csv.reader(open("Results/2trlta1halo.csv","rb"))
outp=csv.writer(open("Results/2trlta1halof.csv","wb"))
outp.writerow("SkyId,pred_x1,pred_y1,pred_x2,pred_y2,pred_x3, pred_y3".split(","))
for sky in range(101,201):
	i=inp.next()
	outp.writerow(["Sky"+str(sky),i[0],i[1]])
