import csv
import subprocess
import sys
for i in range(201,301):
	f=open("Training_Sky"+str(i)+".par","wb")
	f.write('''runmode
        reference 3 0 0
        verbose 0
        inverse 4 0.5 150
        end
source
        grid 0
        n_source 800
        z_source 1.0
        elip_max 0
        end
image
	arcletstat 6 0 Training_Sky'''+str(i)+""".csv
	sigell 0.3
	end 
grille
        nlens     3
        nlens_opt 3
        end
potentiel 1
        profil     12
        x_centre    14.2
        y_centre     3.19
        concentration 3.4
        m200 8e14
        z_lens     0.586
        end
limit 1
        x_centre 1 -103. 103. 0.001
        y_centre 1 -103. 103. 0.001
        end
potentiel 2
        profil     12
        x_centre     55.500
        y_centre     15.0
        m200 8.0e13
        concentration 5.96 
        z_lens     0.341
        end
limit 2
        x_centre 1 -103. 103. 0.001
        y_centre 1 -103. 103. 0.001
        end
potentiel 3
        profil     12
        x_centre     55.500
        y_centre     15.0
        m200 8.0e13
        concentration 5.96 
        z_lens     0.341
        end
limit 3
        x_centre 1 -103. 103. 0.001
        y_centre 1 -103. 103. 0.001
        end
cosmology
        H0        72.0
        omega     0.27
        lambda    0.73
        end
champ
        xmin -200.
        xmax 200.
        ymin -200.
        ymax 200.
       end
fini""")
	f.close()
ans=[]
for i in range(201,301):
    print i
    subprocess.call(["lenstool", "Training_Sky"+str(i)+".par","-n"])
    reader=csv.reader(open("pot.dat","rb"),delimiter=" ")
    reader.next()
    pans1=reader.next()
    x1=float(pans1[1])
    y1=float(pans1[2])
    galaxy_x1=x1*3600/(.05*-1)+2100
    galaxy_y1=y1*3600/(.05)+2100
    reader.next()
    reader.next()
    reader.next()
    pans2=reader.next()
    x2=float(pans2[1])
    y2=float(pans2[2])
    galaxy_x2=x2*3600/(.05*-1)+2100
    galaxy_y2=y2*3600/(.05)+2100
    reader.next()
    reader.next()
    reader.next()
    pans3=reader.next()
    x3=float(pans3[1])
    y3=float(pans3[2])
    galaxy_x3=x3*3600/(.05*-1)+2100
    galaxy_y3=y3*3600/(.05)+2100
    ans.append(["Sky"+str(i),galaxy_x1,galaxy_y1,galaxy_x2,galaxy_y2,galaxy_x3,galaxy_y3])
print ans
writer=csv.writer(open("lenstooltraining3halo.csv","wb"))
writer.writerow("SkyId,pred_x1,pred_y1,pred_x2,pred_y2,pred_x3, pred_y3".split(","))
writer.writerows(ans)