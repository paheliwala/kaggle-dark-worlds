"""Preprocesses data for lenstool"""
import csv
import math
from scipy import arctan, arctan2
pi=3.14159
for i in range(101,102):
	#reader=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(i)+".csv","rb"))
	reader=csv.reader(open("Training_Sky_a"+str(i)+".csv","rb"))
	reader.next()
	op=[]
	for galaxy in reader:
		#print galaxy
		galaxy_x=float(galaxy[1])
		galaxy_y=float(galaxy[2])
		galaxy_e1=float(galaxy[3])
		galaxy_e2=float(galaxy[4])
		x = -1*(galaxy_x-2100) *0.05/ 3600.
		#galaxy_x=x*3600/(.05*-1)+2100
		y =(galaxy_y - 2100)*0.05/3600.
		a = 1/(1-math.sqrt(galaxy_e1**2+galaxy_e2**2))
		b=1/(1+math.sqrt(galaxy_e1**2+galaxy_e2**2))
		#t = arctan(galaxy_e2/galaxy_e1)*180/pi/2.
		#print galaxy_e1,galaxy_e2,math.atan2(galaxy_e2,galaxy_e1)*180/pi/2,pi
		theta=math.atan2(galaxy_e2,galaxy_e1)*180/pi/2.
		op.append([int(galaxy[0][6:]), x,y,a,b,theta, 1.0, 25.5])
		#print op
	writer=csv.writer(open("Training_Sky_p"+str(i)+".csv","wb"),delimiter=' ')
	writer.writerows(op)
