import csv
import subprocess
import sys
for i in range(101,102):
	f=open("Training_Sky"+str(i)+".par","wb")
	f.write('''runmode
        reference 3 0 0
        verbose 0
        inverse 4 0.5 100
        end
source
        grid 0
        n_source 800
        z_source 1.0
        elip_max 0
        end
image
	arcletstat 6 0 Training_Sky_p'''+str(i)+""".csv
	sigell 0.3
	end 
grille
        nlens     1
        nlens_opt 1
        end
potentiel 1
        profil     12
        x_centre    14.2
        y_centre     3.19
        concentration 3.4
    	m200 8e14
        z_lens     0.586
        end

limit 1
        x_centre 1 -103. 103. 0.001
        y_centre 1 -103. 103. 0.001
        end
cosmology
        H0        72.0
        omega     0.27
        lambda    0.73
        end
champ
        xmin -200.
        xmax 200.
        ymin -200.
        ymax 200.
       end
fini""")
	f.close()
ans=[]
for i in range(101,102):
	print i
	subprocess.call(["lenstool", "Training_Sky"+str(i)+".par","-n"])
	reader=csv.reader(open("pot.dat","rb"),delimiter=" ")
	reader.next()
	pans=reader.next()
	x=float(pans[1])
	y=float(pans[2])
	galaxy_x=x*3600/(.05*-1)+2100
	galaxy_y=y*3600/(.05)+2100
	ans.append(["Sky"+str(i),galaxy_x,galaxy_y,0,0,0,0])
print ans
writer=csv.writer(open("lttemp.csv","wb"))
#writer.writerow("SkyId,pred_x1,pred_y1,pred_x2,pred_y2,pred_x3, pred_y3".split(","))
writer.writerows(ans)