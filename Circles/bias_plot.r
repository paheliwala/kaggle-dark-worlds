
rm(list=objects())
#========================================================================
pred_halo <- read.table( 'Results/trci.csv', header=TRUE, sep=',' )
real_halo <- read.table( '../Raw/Training_halos.csv', header=TRUE, sep=',' )
comb_halo <- merge( real_halo, pred_halo )
#========================================================================
# Turn the coordinates into imaginary numbers.
# Makes it easy to do angular computations.
comb_halo$real1  <- comb_halo$halo_x1 + comb_halo$halo_y1*1i
comb_halo$real2  <- comb_halo$halo_x2 + comb_halo$halo_y2*1i
comb_halo$real3  <- comb_halo$halo_x3 + comb_halo$halo_y3*1i
comb_halo$pred1  <- comb_halo$pred_x1 + comb_halo$pred_y1*1i
comb_halo$pred2  <- comb_halo$pred_x2 + comb_halo$pred_y2*1i
comb_halo$pred3  <- comb_halo$pred_x3 + comb_halo$pred_y3*1i
comb_halo$ref    <- comb_halo$x_ref   + comb_halo$y_ref*1i

#========================================================================

r  <- Mod( comb_halo$real1 - comb_halo$pred1 )
a  <- Arg( comb_halo$real1 - comb_halo$pred1 )

r2 <- Mod( comb_halo$real2 - comb_halo$pred2 )
a2 <- Arg( comb_halo$real2 - comb_halo$pred2 )

r3 <- Mod( comb_halo$real3 - comb_halo$pred3 )
a3 <- Arg( comb_halo$real3 - comb_halo$pred3 )

#========================================================================

F1 <- mean( r )
F2 <- mean( r2[ comb_halo$numberHalo > 1 ] )
F3 <- mean( r3[ comb_halo$numberHalo > 2 ] )

G1 <- (mean( sin(a) )^2 + mean( cos(a) ))^2
G2 <- (mean( sin(a2[ comb_halo$numberHalo > 1 ] ) ))^2 + 
      (mean( cos(a2[ comb_halo$numberHalo > 1 ] ) ))^2
G3 <- (mean( sin(a3[ comb_halo$numberHalo > 2 ] ) ))^2 + 
      (mean( cos(a3[ comb_halo$numberHalo > 2 ] ) ))^2



#========================================================================
compas <- complex( modulus=1, argument=a )

bias <- data.frame( x <- Re( compas ), y <- Im( compas ) )


