# vi: spell spl=en
#
library(randomForest)

rm(list=objects())

train <- read.table( "Features/train.csv", header=FALSE, sep="," )
test  <- read.table( "Features/test.csv",  header=FALSE, sep="," )
tlabels <- as.factor(train[,1])

train      <- train[,-c(1:4)] # Drop labels, i and j
# We need this later to translate back to x y coords.
test_index <- test[,c(2:4)] 
test       <- test[,-c(1:4)]  # Drop labels, i and j

rf <- randomForest( 
    train, tlabels, xtest=test, ntree=100, do.trace=TRUE )
predictions <- levels(tlabels)[rf$test$predicted]

test_index$predicted <- predictions
names( test_index ) <- c('sky','i','j','predicted')
write.table( test_index,
    file='Temp/raw_predictions.csv', 
    row.names=FALSE, 
    col.names=TRUE, sep=',', quote=FALSE )

