#-------------------------------------------------------------------------
library(ggplot2)

#-------------------------------------------------------------------------
# Location of all halo's
real_halos   <- read.table( '../Raw/Training_halos.csv', header=TRUE, sep=',' )
pred_halos   <- read.table( 'Results/trci.csv', header=TRUE, sep=',' )

skies <- pred_halos$SkyId

real_halos <- split( real_halos, real_halos$SkyId )
pred_halos <- split( pred_halos, pred_halos$SkyId )

score <- data.frame()
for ( sky in skies[1:300] ) {
    rh <- real_halos[[sky]]
    ph <- pred_halos[[sky]]
    dx <- rh$halo_x1 - ph$pred_x1
    dy <- rh$halo_y1 - ph$pred_y1
    r <- sqrt(dx*dx + dy*dy)
    score <- rbind( score, data.frame( SkyId=sky, r=r ) )
}

#-------------------------------------------------------------------------
print( mean( score$r ) )
# p <- ggplot(score,aes(x=SkyId,y=r))+ geom_bar()
#
# ggsave( plot=p, file="score.pdf" )

