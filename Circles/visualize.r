# vi: spell spl=en
library( ggplot2 )
rm(list=objects())
#======================================================================

halos   <- read.table( '../Raw/Training_halos.csv', header=TRUE, sep=',' )
rp      <- read.table( "Temp/raw_predictions.csv", header=TRUE, sep="," )

pdf( 'Plots/train.pdf' )
for ( sky_rp in split( rp, f=rp$sky ) ) {
    skyid <- sprintf("Sky%d", sky_rp[1,'sky'] ) 
    this_halos <- halos[ halos$SkyId == skyid, ]
    this_halos$halo_x1 <- this_halos$halo_x1/100
    this_halos$halo_y1 <- this_halos$halo_y1/100
    this_halos$halo_x2 <- this_halos$halo_x2/100
    this_halos$halo_y2 <- this_halos$halo_y2/100
    this_halos$halo_x3 <- this_halos$halo_x3/100
    this_halos$halo_y3 <- this_halos$halo_y3/100

    p <- ggplot( sky_rp, aes(x=i,y=j,fill=factor( predicted ) ) ) + 
            geom_tile() +
            geom_point( 
                data=this_halos,
                aes( x=halo_x1,
                     y=halo_y1 ), size=5, 
                     colour='blue', fill='black' ) +
            ggtitle( label=skyid )
    if ( this_halos$halo_y2 != 0 ) {
        p <- p + geom_point( 
            data=this_halos,
            aes( x=halo_x2, y=halo_y2 ), 
            fill='black', colour='white', size=10, alpha=0.4 )

        p <- p + geom_point( 
            data=this_halos,
                aes( x=halo_x2, 
                     y=halo_y2), size=5, colour='white', fill='black' )
    }
    if ( this_halos$halo_y3 != 0 ) {
        p <- p + geom_point( 
            data=this_halos,
            aes( x=halo_x3, y=halo_y3 ), 
            fill='black', colour='white', size=10, alpha=0.4 )
    }

    print(  p )
}
dev.off()

