# vi: spell spl=en
#
rm(list=objects())
library(ggplot2)
library(reshape2)

#-------------------------------------------------------------------

args <- commandArgs( TRUE )
kind <- args[1]

#-------------------------------------------------------------------
# Location of all halo's
halos   <- read.table( '../Raw/Training_halos.csv', header=TRUE, sep=',' )

mk_heatmap <- function( sky, kind ) {
    if ( kind == 'train' ) {
        path <- sprintf( "HeatMaps/Training_Sky%d.csv", sky )
    } else if ( kind == 'anti' ) {
        path <- sprintf( "AHeatMaps/Training_Sky%d.csv", sky )
    } else {
        path <- sprintf( "THeatMaps/Test_Sky%d.csv", sky )
    }
    features <- read.table( file=path, header=TRUE, sep=',' )
    features$f_mean <- scale( features$f_mean )
    features$gal_count <- scale( features$gal_count )
    features$inter_gal <- scale( features$inter_gal )
    features<-melt(features,id.vars=c('x','y'))
    
    sky_id <- sprintf( "Sky%d", sky )
    this_halos <- halos[ halos$SkyId == sky_id, ]

    p <- ggplot( features, aes(x=x,y=y,fill=value ) ) +
        geom_tile() +
        scale_fill_gradient2(low='red',mid='black', high='green') +
        facet_wrap(~variable,nrow=2,ncol=2)
        ggtitle(label=path)

    if ( kind != 'test' ) {
        p <- p + geom_point( 
            data=this_halos,
            aes( x=halo_x1, y=halo_y1 ), 
            fill='black', colour='white', size=10, alpha=0.4 )
            p <- p + geom_point( 
                data=this_halos,
                    aes( x=halo_x1, 
                         y=halo_y1), size=5, colour='blue', fill='black' )
        if ( this_halos$halo_y2 != 0 ) {
            p <- p + geom_point( 
                data=this_halos,
                aes( x=halo_x2, y=halo_y2 ), 
                fill='black', colour='white', size=10, alpha=0.4 )

            p <- p + geom_point( 
                data=this_halos,
                    aes( x=halo_x2, 
                         y=halo_y2), size=5, colour='white', fill='black' )
        }
        if ( this_halos$halo_y3 != 0 ) {
            p <- p + geom_point( 
                data=this_halos,
                aes( x=halo_x3, y=halo_y3 ), 
                fill='black', colour='white', size=10, alpha=0.4 )
        }
    }
    p <- p + ggtitle(sprintf("Sky%d",sky))

    return( p )
}

if ( kind == 'test' ) {
    pdf( "test_combined.pdf" )
    for ( sky in seq(41,120) ) {
        print( mk_heatmap( sky, kind ) )
    }
    dev.off()
} else if ( kind == 'anti' ) {
    pdf( "anti_combined.pdf" )
    for ( sky in seq(102,200) ) {
        print( mk_heatmap( sky, kind ) )
    }
    dev.off()
} else {
    pdf( "train_combined.pdf" )
    for ( sky in seq(1,240) ) {
        print( mk_heatmap( sky, 'train' ) )
    }
    dev.off()
}
