# vi: spell spl=en
#======================================================================

rm(list=objects())

#======================================================================

halos <- read.table( '../Raw/Training_halos.csv', header=TRUE, sep=',' )

predictions <- data.frame(
    SkyId = sprintf( "Sky%d", 1:300 ),
    pred_x1=0.0,
    pred_y1=0.0,
    pred_x2=0.0,
    pred_y2=0.0,
    pred_x3=0.0,
    pred_y3=0.0 )

rp <- read.table( "Temp/raw_predictions.csv", header=TRUE, sep="," )
rp <- rp[rp$predicted>0,]

for ( sky_rp in split( rp, f=rp$sky ) ) {
    skyid <- sprintf("Sky%d", sky_rp[1,'sky'] ) 
    this_halos <- halos[ halos$SkyId == skyid, ]

    # Always predict the first halo
    sky_rp_1 <- sky_rp[ sky_rp$predicted == 1, ]
    # TODO 100 should be read from parameter file.
    x <- 100 * mean( sky_rp_1$i ) + 50
    y <- 100 * mean( sky_rp_1$j ) + 50
    predictions[ predictions$SkyId == skyid, 'pred_x1' ] <- x
    predictions[ predictions$SkyId == skyid, 'pred_y1' ] <- y

    if (this_halos$numberHalos > 1 ) {
        sky_rp_2 <- sky_rp[ sky_rp$predicted == 2, ]
        x <- 100 * mean( sky_rp_2$i ) + 50
        y <- 100 * mean( sky_rp_2$j ) + 50
        predictions[ predictions$SkyId == skyid, 'pred_x2' ] <- x
        predictions[ predictions$SkyId == skyid, 'pred_y2' ] <- y
    }
    if (this_halos$numberHalos > 2 ) {
        sky_rp_3 <- sky_rp[ sky_rp$predicted == 3, ]
        x <- 100 * mean( sky_rp_3$i ) + 50
        y <- 100 * mean( sky_rp_3$j ) + 50
        predictions[ predictions$SkyId == skyid, 'pred_x3' ] <- x
        predictions[ predictions$SkyId == skyid, 'pred_y3' ] <- y
    }

}

#======================================================================

write.table( 
    predictions, 
    file='Results/trci.csv',
    col.names=TRUE, row.names=FALSE, quote=FALSE, sep=',' )

#======================================================================

