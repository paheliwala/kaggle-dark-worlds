/* vi: spell spl=en 
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

//-------------------------------------------------------------------
void unittest( void );

//------------------------------------------------------------------
//--------------------------------------------------------------------
/* To hold the data from Sky files */
struct Galaxy {
    double x;
    double y;
    double e1;
    double e2;
    struct Galaxy* next;        /* Linked list for the whole set */
    struct Galaxy* subset_next; /* Linked list for a subset */
};

struct Halo {
    int    count;
    double x1;
    double y1;
    double x2;
    double y2;
    double x3;
    double y3;
    struct Halo* next;        /* Linked list for the whole set */
};

// Training sky has the maximum number of halo's, 300
#define MAX_HALOS (300)
struct Halo* halo_index[ MAX_HALOS + 2 ]; /* Add some head room */


//-------------------------------------------------------------------
//-------------------------------------------------------------------
void show( struct Galaxy* g )
{
    for ( ; g; g = g->next ) {
        printf("%f %f %f %f\n", g->x, g->y, g->e1, g->e2 );
    }
}

void show_subset( struct Galaxy* g )
{
    for ( ; g; g = g->subset_next ) {
        printf("%f %f %f %f\n", g->x, g->y, g->e1, g->e2 );
    }
}


double distance( double x1, double y1, double x2, double y2 )
{
    double dx = x1 - x2;
    double dy = y1 - y2;
    return ( sqrt( dx*dx + dy*dy ) );
}

struct Galaxy* near( 
        struct Galaxy *g, double xref, 
        double yref, double r )
{
    struct Galaxy* subset = NULL;
    for ( ; g; g = g->next ) {
        if ( distance( xref, yref, g->x, g->y ) < r ) {
            g->subset_next = subset;
            subset = g;
        }
    }
    return subset;
}

#define READ_BUFFER_SIZE (1024)

struct Halo* read_halos( char * filename )
{
    struct Halo* r = NULL;
    FILE *in;
    char buffer[READ_BUFFER_SIZE+1];
    int index = 1;  /* Start at one cause skies are numbered from 1 */

    in = fopen( filename, "r" );
    if ( in ) {
        char* s;
        // Skip header
        s = fgets( buffer, READ_BUFFER_SIZE, in );
        while ( fgets( buffer, READ_BUFFER_SIZE, in ) ) {
            struct Halo* h = malloc( sizeof( struct Halo ) );

            s = strtok( buffer, "," ); // Name of galaxy...
            s = strtok( NULL, "," ); 
            h->count = atoi(s);
            assert( h->count > 0 ); assert( h->count < 4 );
            s = strtok( NULL, "," ); // xref Not used
            s = strtok( NULL, "," ); // yref Not used
            s = strtok( NULL, "," );
            h->x1 = atof(s);
            s = strtok( NULL, "," );
            h->y1 = atof(s);
            s = strtok( NULL, "," );
            h->x2 = atof(s);
            s = strtok( NULL, "," );
            h->y2 = atof(s);
            s = strtok( NULL, "," );
            h->x3 = atof(s);
            s = strtok( NULL, "," );
            h->y3 = atof(s);
            // Insert into the list
            h->next = r;
            assert( index <= MAX_HALOS );
            halo_index[ index ] = h;
            index++;
            r = h;
        }
    }
    return r;
}


struct Galaxy* read_galaxies( char* filename )
{
    struct Galaxy* r = NULL;
    FILE *in;
    char buffer[READ_BUFFER_SIZE+1];

    in = fopen( filename, "r" );
    if ( in ) {
        char* s;
        // Skip header
        s = fgets( buffer, READ_BUFFER_SIZE, in );
        while ( fgets( buffer, READ_BUFFER_SIZE, in ) ) {
            struct Galaxy* g = malloc( sizeof( struct Galaxy ) );
            // Name of galaxy...
            s = strtok( buffer, "," );
            s = strtok( NULL, "," );
            g->x = atof(s);
            s = strtok( NULL, "," );
            g->y = atof(s);
            s = strtok( NULL, "," );
            g->e1 = atof(s);
            s = strtok( NULL, "," );
            g->e2 = atof(s);
            g->next = r;
            g->subset_next = NULL;
            r = g;
        }
    }
    return r;
}


// Just one feature for now.
struct FeatureSet {
    // How much are the galaxies pointing to the center of the circle.
    double mean_force_dev; 
    // Number of galaxies in the circle
    double gal_count;
    // Minimum distance to the next galaxy for all galaxies in
    // the circle.
    double inter_gal;
};

//==============================================================
struct FeatureSet compute_features( 
        struct Galaxy* g, double xref, double yref ) 
{
    struct Galaxy* i_g = NULL;
    struct Galaxy* j_g = NULL;
    struct FeatureSet fs;
    double mean_d = 0.0;
    int n = 0;
    for ( i_g = g ; 
          i_g; 
          i_g = i_g->subset_next ) {
        double force_angle, galaxy_angle, dx, dy, d;
        dx = xref - i_g->x;
        dy = yref - i_g->y;
        force_angle = atan2( dy, dx );
        galaxy_angle = atan2( i_g->e2, i_g->e1 )/2 + M_PI/2;

        d = force_angle - galaxy_angle;
        if ( d < -0.25*M_PI ) {
            d = d + 2*M_PI;
        }
        if ( d > 0.25*M_PI ) {
            d = d - M_PI;
        }
        d = fabs( d );

        mean_d += d;
        n++;
    }
    fs.mean_force_dev = mean_d/n;
    fs.gal_count = n;

    double r = 0.0;
    double mean = 0.0;
    double M2 = 0.0;

    n = 0;
    for ( i_g = g;
          i_g;
          i_g = i_g->subset_next ) {
        double r_min = 9999.0;
        double delta;
        for ( j_g = g ; 
              j_g; 
              j_g = j_g->subset_next ) {
            if ( j_g != i_g ) {
                double dx, dy;
                dx = j_g->x - i_g->x;
                dy = j_g->y - i_g->y;
                r = dx*dx + dy*dy;
                if ( r < r_min ) {
                    r_min = r;
                }
            }
        }
        n += 1;
        delta = r_min - mean;
        mean = mean + delta/n;
        M2 = M2 + delta*(r_min - mean);
    }
    fs.inter_gal = M2/(n-1);

    return fs;
}

//==============================================================
double get_hm_value( double *heat_map, int i, int j, int m ) 
{
    if ( ( i >= 0 ) && ( j >= 0 ) &&
         ( i < m  ) && ( j < m  ) ) {
            return heat_map[i+j*m];
    } else {
        /* Outside the heat map */
        return 0.0;
    }
}

//==============================================================
double* compute_heat_map( 
        FILE* out, struct Galaxy* galaxies,
        int step, int xy_max, int m 
) {
    double xref;
    double yref;
    double* heat_map = NULL;

    int i,j;
    int n = 0;
    heat_map = calloc( m*m, sizeof(double) );

    if ( out ) {
        fprintf( out, "%s\n", "x,y,f_mean,gal_count,inter_gal" );
    }
    for ( xref = 0, i = 0; 
            xref <= xy_max; 
            xref = xref + step, ++i ) {
        for ( yref = 0, j = 0;
                yref <= xy_max; 
                yref = yref + step, j++ ) {
            struct FeatureSet fs;

            fs = compute_features( 
                    near( galaxies, xref, yref, 1000 ),
                    xref, yref );

            if ( out ) {
                fprintf( out, "%f,%f,%f,%f,%f\n", 
                        xref, yref, 
                        fs.mean_force_dev,
                        fs.gal_count,
                        fs.inter_gal );
            }
            heat_map[i+j*m] = fs.mean_force_dev;
            ++n;
        }
    }

    return heat_map;
}


//==============================================================

int xy_to_ij( double xy, int step )
{
    int r = floor( xy );
    if ( r >= 0 ) {
        r = r / step;
    } else {
        r = (r + 1) / step - 1;
    }
    return r;
}

//==============================================================

double ij_to_xy( int ij, int step )
{
    double d = step * 0.5;
    double r = floor( ij );
    if ( r >= 0 ) {
        r = r * step - d;
    } else {
        r = r * step + d;
    }
    return r;
}

//==============================================================
int get_feature_set( double* features, double* heat_map, 
        int ci, int cj, 
        int delta, int m )
{
    int index = 0;
    for ( int i = -delta + 1; i < delta; i++ ) {
        for ( int j = -delta + 1; j < delta; j++ ) {
            features[ index ] = get_hm_value( heat_map, ci + i, cj + j, m );
            index++;
        }
    }
    return index;
}

//==============================================================

void create_feature_line( 
        FILE* out, int ci, int cj, int n, double* features, int label, int sky )
{
    fprintf( out, "%d,%d,%d,%d,", label, sky, ci, cj );
    for ( int i = 0; i < n-1; i++ ) {
        fprintf( out, "%.5f,", features[ i ] );
    }
    fprintf( out, "%.3f\n", features[ n-1 ] );
}

//==============================================================
void create_feature_file( 
       FILE* out, double* heat_map, 
       int step, int m,
       int sky, bool train )
{
    int delta = 6;
    double* features = calloc( (2*delta-1)*(2*delta-1), sizeof( double ) );

    if ( train ) {
        struct Halo* h = halo_index[ sky ];

        for ( int ci = -delta; ci < m + delta; ci++ ) {
            for ( int cj = -delta; cj < m + delta; cj++ ) {
                int n = get_feature_set( features, heat_map, ci, cj, delta, m );
                bool near_halo = false;
                for ( int h_id = 0; h_id < h->count; h_id++ ) {
                    double x = -9000;
                    double y = -9000;
                    if ( h_id == 0 ) {
                        x = h->x1;
                        y = h->y1; 
                    } else if ( h_id == 1 ) {
                        x = h->x2;
                        y = h->y2; 
                    } else if ( h_id == 2 ) {
                        x = h->x3;
                        y = h->y3; 
                    }
                    assert( x > -8000 );
                    if ( distance( x, y, ij_to_xy( ci, step ), 
                                ij_to_xy( cj, step ) ) < 1500 ) {
                        near_halo = true;
                    }
                }
                if ( !near_halo ) {
                    if ( drand48() < 0.005 ) {
                        create_feature_line( out, ci, cj, n, features, 0, sky );
                    }
                }
            }
        }

        for ( int h_id = 0; h_id < h->count; h_id++ ) {
            double x = -9000;
            double y = -9000;
            if ( h_id == 0 ) {
                x = h->x1;
                y = h->y1; 
            } else if ( h_id == 1 ) {
                x = h->x2;
                y = h->y2; 
            } else if ( h_id == 2 ) {
                x = h->x3;
                y = h->y3; 
            }
            assert( x > -8000 );
            int ci = xy_to_ij( x, step );
            int cj = xy_to_ij( y, step );
            int n = get_feature_set( features, heat_map, ci, cj, delta, m );
            create_feature_line( out, ci, cj, n, features, h_id+1, sky );
        }

    } else {
        for ( int ci = -delta; ci < m + delta; ci++ ) {
            for ( int cj = -delta; cj < m + delta; cj++ ) {
                int n = get_feature_set( features, heat_map, ci, cj, delta, m );
                int label = 0;
                create_feature_line( out, ci, cj, n, features, label, sky );
            }
        }
    }
}

void create_trainingset( void )
{
    FILE* out = fopen( "Features/train.csv", "w" );
    if ( out ) {
        struct Galaxy* galaxies;
        struct Halo*   halos;
        halos    = read_halos( "../Raw/Training_halos.csv" ); 
        for ( int sky = 1; sky <= 300; sky++ ) {
            char buffer[READ_BUFFER_SIZE+1];
            sprintf( buffer, "../Raw/Train_Skies/Training_Sky%d.csv", sky );
            printf( "Processing %s\n", buffer );
            galaxies = read_galaxies( buffer ); 
            double* heat_map = NULL;
            int step   = 100;
            int xy_max = 4000;
            // Dimension of the heap map
            int m = xy_max/step + 1;
            heat_map = compute_heat_map( NULL, galaxies, step, xy_max, m );
            create_feature_file( out, heat_map, step, m, sky, true );
        }
        fclose( out );
    } else {
        fprintf( stderr, "Can't open Features/train.csv" );
    }
}


void create_testset( void )
{
    FILE* out = fopen( "Features/test.csv", "w" );
    if ( out ) {
        struct Galaxy* galaxies;
        for ( int sky = 1; sky <= 300; sky++ ) {
            char buffer[READ_BUFFER_SIZE+1];
            sprintf( buffer, "../Raw/Train_Skies/Training_Sky%d.csv", sky );
//            sprintf( buffer, "../Raw/Test_Skies/Test_Sky%d.csv", sky );
            printf( "Processing %s\n", buffer );
            galaxies = read_galaxies( buffer ); 
            double* heat_map = NULL;
            int step   = 100;
            int xy_max = 4000;
            // Dimension of the heap map
            int m = xy_max/step + 1;
            heat_map = compute_heat_map( NULL, galaxies, step, xy_max, m );
            create_feature_file( out, heat_map, step, m, sky, false );
            free( heat_map );
        }
        fclose( out );
    } else {
        fprintf( stderr, "Can't open Features/train.csv" );
    }
}

//==============================================================
int main( int argc, char** argv )
{
    struct Galaxy* galaxies;
    struct Halo*   halos;
    int result = EXIT_FAILURE;

    if ( argc < 3 ) {
        if ( ( argc == 2 ) && ( strcmp( argv[1], "unittest" ) == 0 ) ) {
            unittest();
        } else if ( ( argc == 2 ) && ( strcmp( argv[1], "train" ) == 0 ) ) {
            create_trainingset();
            result = EXIT_SUCCESS;
        } else if ( ( argc == 2 ) && ( strcmp( argv[1], "test" ) == 0 ) ) {
            create_testset();
            result = EXIT_SUCCESS;
        } else {
            // TODO Usage
            fprintf( stderr, "Too few parameters" );
        }
    } else { // All is OK
        FILE *out;
        galaxies = read_galaxies( argv[1] ); 
        halos    = read_halos( "../Raw/Training_halos.csv" ); 
        out = fopen( argv[2], "w" );
        if ( out ) {
            double* heat_map = NULL;
            int step   = 100;
            int xy_max = 4000;
            // Dimension of the heap map
            int m = xy_max/step + 1;

            heat_map = compute_heat_map( 
                   out, galaxies,
                   step, xy_max, m );
            fclose( out );

            out = fopen( "circle_par.csv", "w" );
            if ( out ) {
                fprintf( out, "%s\n", "xy_max,step,m" );
                fprintf( out, "%d,%d,%d\n", xy_max, step, m );
                fclose( out );
                result = EXIT_SUCCESS;
            } else {
                fprintf( stderr, "Can't open circle_par\n" );
            }
        } else {
            fprintf( stderr, "Can't open %s\n", argv[2] );
        }
    }
    return result;
}


//==============================================================

void unittest( void ) 
{
    struct Galaxy* galaxies;
    struct Halo*   halos;
    struct Halo*   h133;

    double* heat_map = NULL;
    int step   = 100;
    int xy_max = 4000;
    // Dimension of the heap map
    int m = xy_max/step + 1;

    halos    = read_halos( "../Raw/Training_halos.csv" ); 
    assert( halos != NULL );
    assert( halo_index[ 133 ] != NULL );
    h133 = halo_index[ 133 ];
    assert( h133->x1 - 2301.69 < 0.01 );
    assert( h133->y1 - 2380.04 < 0.01 );

    galaxies = read_galaxies( "../Raw/Train_Skies/Training_Sky133.csv" ); 
    assert( galaxies != NULL );

    {
        FILE *out;
        out = fopen( "/dev/null", "w" );
        heat_map = compute_heat_map( 
               out, galaxies,
               step, xy_max, m );
        assert( heat_map != NULL );
    }

    {
        assert( get_hm_value( heat_map, -1, -1, m ) == 0.0 );
        assert( get_hm_value( heat_map, 0, -1, m ) == 0.0 );
        assert( get_hm_value( heat_map, m, m, m ) == 0.0 );
        assert( get_hm_value( heat_map, m+10, m, m ) == 0.0 );
        assert( get_hm_value( heat_map, 10, 10, m ) > 0.2 );
    }

    {
        assert( xy_to_ij( 10,   step ) == 0 );
        assert( xy_to_ij( 100,  step ) == 1 );
        assert( xy_to_ij( 101,  step ) == 1 );
        assert( xy_to_ij( 0,    step ) == 0 );
        assert( xy_to_ij( -1,   step ) == -1 );
        assert( xy_to_ij( -100, step ) == -1 );
        assert( xy_to_ij( -101, step ) == -2 );
        assert( xy_to_ij( -200, step ) == -2 );
        assert( xy_to_ij( -201, step ) == -3 );
    }

    {
        int ci = xy_to_ij( h133->x1, step );
        int cj = xy_to_ij( h133->y1, step );
        int delta = 4;
        double* features = calloc( (2*delta-1)*(2*delta-1), sizeof( double ) );
        int index = get_feature_set( features, heat_map, ci, cj, delta, m );
        assert( index == 49 );
        printf( "%d\n", index );
        for ( int i = 0; i < index; i++ ) {
            assert( features[ i ] > 0.0 );
        }
    }

    {
        int ci = 0;
        int cj = 20;
        int delta = 4;
        double* features = calloc( (2*delta-1)*(2*delta-1), sizeof( double ) );
        int index = get_feature_set( features, heat_map, ci, cj, delta, m );
        assert( index == 49 );
    }

    {
        FILE* out;
        out = fopen( "/tmp/test.csv", "w" );
        assert( out );
        create_feature_file( out, heat_map, step, m, 133, true );
        fclose( out );
    }

    printf( "Done:\n" );
}

