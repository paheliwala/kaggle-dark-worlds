import numpy as np
from sklearn import svm
from sklearn import cross_validation
from sklearn.preprocessing import *
from sklearn.grid_search import GridSearchCV
import time
import csv
a=time.time()

X_train=[]
y_train=[]
X_test=[]
y_test=[]
for sky in range(1,20):
	galaxies=csv.reader(open("../processed/sky"+str(sky)+".csv","rb"))
	galaxies.next()
	for galaxy in galaxies:
		galaxy=[float(i) for i in galaxy]
		X_train.append(galaxy[4:])
		y_train.append(int(galaxy[1]))
print "Data read",time.time()-a
#parameters={"gamma":[1e-10,1e-6,1e-5,.0001,.0003], "C":[1e-10,1e-6,1e-5,.0001,.0003], "degree":[2]} #Optimal parameter grid
#parameters={"gamma":[0.1,1], "C":[1],"degree":[3]}
parameters={"kernel":["rbf","rbf"]}
scaler=StandardScaler()
X_train_scaled=scaler.fit_transform(X_train)

clf=svm.SVC(kernel="rbf")
clfgs = GridSearchCV(estimator=clf, param_grid=parameters, verbose=True,cv=3)
clfgs.fit(X_train_scaled, y_train)

print "GridSearch completed",time.time()-a
#print clfgs.best_estimator_
print "Best score: %0.5f" % clfgs.best_score_
print "Best parameters set:"

best_parameters = clfgs.best_estimator_.get_params()
for param_name in sorted(parameters.keys()):
    print "\t%s: %r" % (param_name, best_parameters[param_name])
