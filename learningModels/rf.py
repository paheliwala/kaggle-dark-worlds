import numpy as np
from time import time
from sklearn.svm import SVC
from sklearn import metrics
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
#from sklearn
import csv
import pylab as p
from matplotlib.backends.backend_pdf import PdfPages
import math
from numpy import linspace
from scipy import pi,sin,cos
np.random.seed(1) 
def ellipse(ra,rb,ang,x0,y0,Nb=50):
	'''ra - major axis length
 rb - minor axis length
 ang - angle
 x0,y0 - position of centre of ellipse
 Nb - No. of points that make an ellipse
'''
	xpos,ypos=x0,y0
	radm,radn=ra,rb
	an=ang

	co,si=cos(an),sin(an)
	the=linspace(0,2*pi,Nb)
	X=radm*cos(the)*co-si*radn*sin(the)+xpos
	Y=radm*cos(the)*si+co*radn*sin(the)+ypos
	return X,Y

X_train=[]
y_train=[]
X_test=[]
y_test=[]
a=time()
for sky in range(1,101):
	galaxies=csv.reader(open("processed/sky"+str(sky)+".csv","rb"))
	galaxies.next()
	for galaxy in galaxies:
		#print galaxy
		galaxy=[float(i) for i in galaxy]
		X_train.append(galaxy[8:])
		y_train.append(int(galaxy[1]))
#print X_train[:5]
#print y_train[:5]

for sky in range(41,81):
	galaxies=csv.reader(open("processed/test/sky"+str(sky)+".csv","rb"))
	galaxies.next()
	for galaxy in galaxies:
		#print galaxy
		galaxy=[float(i) for i in galaxy]
		X_test.append(galaxy[8:])
		y_test.append(int(galaxy[1]))
#print X_test[:5]
#print y_test[:5]
print "Data Read",time()-a

scaler=preprocessing.StandardScaler()
X_train_scaled=scaler.fit_transform(X_train)
X_test_scaled=scaler.transform(X_test)
print "Scaled data", time()-a
#
#clf=SVC()
clf=RandomForestClassifier()
clf.fit(X_train,y_train)
#clf.fit(X_train_scaled,y_train)
print "Model fitted",time()-a

preds=clf.predict(X_test)
#preds=clf.predict(X_test_scaled)
"Predictions done",time()-a
#writer=csv.writer(open("Results/2trrf.py","wb"))
#writer.writerow(["nearhalo"])
#writer.writerows([preds])
print metrics.classification_report(y_test,preds)
print metrics.zero_one_score(y_test,preds)
print metrics.confusion_matrix(y_test,preds)

#Uncomment below for plotting

halos_file=csv.reader(open("../lenstool/Results/2telt.csv","rb"))
#halos_file=csv.reader(open("../Raw/Training_halos.csv","rb"))
halos_file.next()
halos=[i for i in halos_file]
scale=40
ipreds=0
preds_file=csv.writer(open("Results/2terf.csv","wb"))
preds_file.writerow("SkyId,pred_x1,pred_y1,pred_x2,pred_y2,pred_x3,pred_y3".split(","))
for i in range(41,81):
	print "Processing sky"+str(i)
	fig = p.figure(figsize=(8,8))
	p.axis([0,4200,0,4200])

	sky=csv.reader(open("processed/test/sky"+str(i)+".csv","rb"))
	sky.next()
	halopred=[]
	for galaxy in sky:
		x=float(galaxy[2])		
		y=float(galaxy[3])
		e1=float(galaxy[4])
		e2=float(galaxy[5])
		#print x,y,e1,e2
		#exit()
		e=math.sqrt(e1**2+e2**2)
		a = 1/(1.-e)*scale
		b=1/(1.+e)*scale
		theta=math.atan2(e2,e1)/2.
		#print theta
		X,Y=ellipse(a,b,theta,x,y,Nb=20)
		 
		if preds[ipreds]==1:
			color="r" 
			halopred.append([x,y])
		elif int(galaxy[1]):
		 	color="g"
		else:
		 	color="k"
		p.plot(X,Y,color+".-",ms=1) # black ellipse
		ipreds+=1
	halopred=np.array(halopred)
	if len(halopred)==0:
		halopred=np.array([[2100,2100],[2100,2100]])
	halo=halopred.mean(axis=0)
	#print halo
	X,Y=ellipse(100,100,pi/2,halo[0],halo[1],Nb=20)
	preds_file.writerow(["Sky"+str(i),halo[0],halo[1],0,0,0,0])
	p.plot(X,Y,"m.-",ms=1) # rf ellip
	print halos[i-41]
	X,Y=ellipse(100,100,pi/2,float(halos[i-41][1]),float(halos[i-41][2]),Nb=20)
	p.plot(X,Y,"c.-",ms=1) # ground truth ellips
	X,Y=ellipse(100,100,pi/2,float(halos[i-41][3]),float(halos[i-41][4]),Nb=20)
	p.plot(X,Y,"c.-",ms=1) # ground truth ellips
	p.savefig("svmtempimages/p"+str(i)+".png")
	p.clf()
	