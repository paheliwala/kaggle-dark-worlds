"""Superimposes average.png with input file"""
import Image, ImageOps, ImageFilter
import numpy as np
from sklearn.cluster import DBSCAN,MeanShift,estimate_bandwidth
from scipy import stats

im=Image.open("images/average.png")
image_file=ImageOps.grayscale(im)
imgarray=np.asarray(image_file)
for i in xrange(41,81):
	print "Processing Sky",str(i)
	#ell=Image.open("../tempimages/test/"+str(i)+".png")
	ell=Image.open("../tempimages/test/"+str(i)+".png")
	"""Optionally scale image brightness"""
	#ella=np.asarray(ell)
	#ella=(ella.max()-ella)*255/ella.max()-ella.min()
	#ell=Image.fromarray(ella)
	blend=Image.blend(ell,image_file,.5)
	blend.save("../tempimages/test/superimposed/"+str(i)+"p.png")
	#blend.save("../tempimages/superimposed/"+str(i)+"pa2.png")

#ell=ImageOps.grayscale(ell).show()
#ell1=ell.filter(ImageFilter.BLUR)
#ell1.show()
#ell2=Image.blend(ell,im,.5)
#ell2.show()
#ellarr=np.asarray(ell)
#ans=(ellarr+imgarray)/2.
#print ans
#output=Image.fromarray(ans)
#output.show()
#ell1=ImageOps.grayscale(ell1)
#ell2=ImageOps.grayscale(ell2)
#print ell2.size,im.size,ell2.mode,im.mode
#blend.show()
#blendarr=np.asarray(blend)
#blendarr=np.delete(blendarr, np.s_[397:], axis=0)
#blendarr=np.delete(blendarr, np.s_[397:], axis=1)
#print np.shape(blendarr)
#normal=(blendarr-blendarr.min())/(float(blendarr.max()-blendarr.min()))*255
#result=np.where(blendarr<blendarr.mean(),blendarr,-1)
#print normal.min()
#print result
#print np.where(result.sum(axis=0)==result.sum(axis=0).min())
#print np.where(result.sum(axis=1)==result.sum(axis=1).min())

#Image.fromarray(normal).show()
#print DBSCAN(eps=1400, min_samples=10).fit_predict(result)
#bandwidth = estimate_bandwidth(result, quantile=0.2, n_samples=500)
#print MeanShift(bandwidth=bandwidth, bin_seeding=True).fit_predict(result)

#Image.fromarray(np.where(blendarr<blendarr.mean(),blendarr,-1))
#print where(stats.mode(blendarr)[0].max()
#print np.where(blendarr.mean(axis=0)==blendarr.mean(axis=0).max())[0][0]*10
#print np.where(blendarr.mean(axis=1)==blendarr.mean(axis=1).max())[0][0]*10
#print stats.mode(blendarr)