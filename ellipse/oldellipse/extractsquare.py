"""This script extracts a square from the tempimages/
The sqaure is the region around lenstool predictions
Imageprocessing algorithm should be run on this region to correct the
predictions of lenstool.

this produces 0.161134248243
float(halo4[1])+ans_x/50.,float(halo4[2])-ans_y/50.

lenstool produces .176237715124"""
import Image, ImageOps, ImageFilter
import numpy as np
from sklearn.cluster import DBSCAN,MeanShift,estimate_bandwidth
from scipy import stats
import csv
import sys
import subprocess
import time
#im=Image.open("foo.png")

#image_file=ImageOps.grayscale(im)
#image_file.show()
#imgarray=np.asarray(image_file)
#print imgarray
corrections=[]

halos4=csv.reader(open("../lenstool/lenstooltraining1halo.csv","rb"))
#halos4=csv.reader(open("../misc/lenstool.benchmark.csv","rb"))
halos4.next()
for i in range(1,4):
	print "Processing Sky"+str(i)
	ell=Image.open("../tempimages/"+str(i)+".png")
	ell=ImageOps.grayscale(ell)
	#p.plot(2337.2184,1176.276,"r+",ms=100)
	#ell.show()
	halo4=halos4.next()
	lensx=int(float(halo4[1]))
	lensy=4200-int(float(halo4[2])) #dist from top of image
	gridsize=500
	
	l=lensx-gridsize
	u=lensy-gridsize
	r=lensx+gridsize
	b=lensy+gridsize
	if( l<0 or u<0 or r>4200 or b>4200):
		gridsize=np.array([lensx,4200-lensy,4200-lensx,lensy]).min()-1
		l=lensx-gridsize
		u=lensy-gridsize
		r=lensx+gridsize
		b=lensy+gridsize
	ell=ell.crop((l,u,r,b))
	ell.save(str(i)+"cropped.png")
	#ell.show()
	#ell.show()
	#exit()
	ellarr=np.asarray(ell)
	#print ellarr
	ans=np.array(np.where(ellarr<=ellarr.min()))
	print len(ans[0])
	if len(ans)<2:
		ans=np.array([[0],[0]])
	#ans_x=np.median(ans[1])-gridsize
	ans_x=np.mean(ans[1]-gridsize)
	#ans_y=-(np.median(ans[0])-gridsize)
	ans_y=-(np.mean((ans[0])-gridsize))
	#print ans_x,ans_y,float(halo4[1])+ans_x/5.,float(halo4[2])+ans_y/5.
	corrections.append(["Sky"+str(i),float(halo4[1])+ans_x/1.,float(halo4[2])+ans_y/1.,0,0,0,0]) 
	#print np.mean([524,478]),np.mean([662,478])
writer=csv.writer(open("corrections.csv","wb"))
writer.writerow("SkyId,pred_x1,pred_y1,pred_x2,pred_y2,pred_x3, pred_y3".split(","))
writer.writerows(corrections)
time.sleep(10)
subprocess.call(["python", "../tools/DarkWorldsMetric.py", "../ellipse/corrections.csv", "../tools/1Training_halos.csv"]) 
