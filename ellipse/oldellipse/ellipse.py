'''
This is a image processing algorithm to find out halo centres
It does following things to a sky:
1.rotate ellipses by 90 degrees
2.scale them to make them bigger
3.sky is reduced to small size to make image dense
4.save the figure

Next step is to find the white clusters in image. That cluster is most likely halo centre
'''
import csv
import pylab as p
from matplotlib.backends.backend_pdf import PdfPages
import math
from numpy import linspace
from scipy import pi,sin,cos


def ellipse(ra,rb,ang,x0,y0,Nb=50):
	'''ra - major axis length
 rb - minor axis length
 ang - angle
 x0,y0 - position of centre of ellipse
 Nb - No. of points that make an ellipse

 based on matlab code ellipse.m written by D.G. Long,
 Brigham Young University, based on the
 CIRCLES.m original
 written by Peter Blattner, Institute of Microtechnology,
 University of
 Neuchatel, Switzerland, blattner@imt.unine.ch
'''
	xpos,ypos=x0,y0
	radm,radn=ra,rb
	an=ang

	co,si=cos(an),sin(an)
	the=linspace(0,2*pi,Nb)
	X=radm*cos(the)*co-si*radn*sin(the)+xpos
	Y=radm*cos(the)*si+co*radn*sin(the)+ypos
	return X,Y

def test():
	#Read Halos
	halos=csv.reader(open("../Raw/Training_halos.csv","rb"))
	halos.next()
	#Read Gridded signal benchmark
	halos2=csv.reader(open("../Gridded_Signal/Gridded_Signal_benchmark.csv","rb"))
	halos2.next()
	#pp = PdfPages('train_gridded.pdf')
	#Read each sky in Training set
	for sky in range(1,100):
		f=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(sky)+".csv","rb"))
		f.next()
		print "Plotting Sky"+str(sky)
		fig = p.figure(figsize=(4,4))
		p.axis([0,4200,0,4200]) #Plot size
		
		scale=500000 #Scale
		
		for line in f:
			#print line
			x=float(line[1])		
			y=float(line[2])
			e1=float(line[3])
			e2=float(line[4])

			e=math.sqrt(e1**2+e2**2)
			a = 1/(1.-e)*scale*0.00000
			b=1/(1.+e)*scale#*10
			theta=math.atan2(e2,e1)/2.
			#print theta
			for k in range(100,101):
				X,Y=ellipse(a*k/100.,b*k/100.,theta,x,y,Nb=50)
				p.plot(X,Y,color=(0,0,0,.2),marker=".",ms=1) # Plot galaxy
		#Uncomment below if you want to pl1t halos
		'''halo=halos.next()
		X,Y=ellipse(200,200,3.1416/2,float(halo[4]),float(halo[5]))
		p.plot(X,Y,"g.-",ms=2) 
		halo2=halos2.next()
		X,Y=ellipse(200,200,3.1416/2,float(halo2[1]),float(halo2[2]))
		p.plot(X,Y,"r.-",ms=2) 
		if float(halo[6])!=0:

			X,Y=ellipse(200,200,3.1416/2,float(halo[6]),float(halo[7]))
			p.plot(X,Y,"g.-",ms=2) 
			X,Y=ellipse(200,200,3.1416/2,float(halo2[3]),float(halo2[4]))
			p.plot(X,Y,"r.-",ms=2) 
		if float(halo[8])!=0:
			X,Y=ellipse(200,200,3.1416/2,float(halo[8]),float(halo[9]))
			p.plot(X,Y,"g.-",ms=2) 
			X,Y=ellipse(200,200,3.1416/2,float(halo2[5]),float(halo2[6]))
			p.plot(X,Y,"r.-",ms=2) '''
		
		p.grid(False) #Don't show grid
		ax = p.gca()
		ax.yaxis.set_visible(False) #Don't show axes
		ax.xaxis.set_visible(False)
		p.tight_layout(pad=0) # No margin
		p.savefig("../tempimages/"+str(sky)+".png")
		p.clf()

if __name__ == '__main__':
	test()