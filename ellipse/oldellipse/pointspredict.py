"""Makes predictions based on points algorithm processed images"""
import numpy as np 
import Image, ImageOps
""" make predictions based on images created by points algo"""
import csv
import subprocess
predictions=[]
for i in xrange(1,100):
	print "Processing Image",i
	ip=Image.open("../tempimages/superimposed/"+str(i)+"p.png")
	ipa=np.asarray(ip)
	ans=np.array(np.where(ipa<=ipa.min()*1.04))
	#print np.shape(ans)
	ans_x=np.mean(ans[1])
	ans_y=4200-(np.mean((ans[0])))
	#print ans_x,ans_y
	predictions.append(["Sky"+str(i),ans_x/1.,ans_y/1.,0,0,0,0]) 

with open("points.csv","wb") as f:
	writer=csv.writer(f)
	writer.writerow("SkyId,pred_x1,pred_y1,pred_x2,pred_y2,pred_x3,pred_y3".split(","))
	writer.writerows(predictions)
#time.sleep(10)
subprocess.call(["python", "../tools/DarkWorldsMetric.py", "../ellipse/points.csv", "../tools/1Training_halos.csv"]) 
