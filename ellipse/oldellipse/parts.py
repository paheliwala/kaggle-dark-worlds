#
#
#
import csv
from PIL import Image 
import sys
from scipy import stats
import numpy as np
import math

image_file = Image.open("../tempimages/thick/1.png")
image_file = image_file.convert('1')
pixels     = image_file.load()

width  = image_file.size[0]
height = image_file.size[1]

def to_1( x ):
    if x == 255:
        return 1
    else:
        return 0

def dump_pixels( image ):
    clust=[]
    for y in range(0,height):
        for x in range(0,width):
            clust.append(image[x][y])
            #print( "%02d" % image[x][y] ),
    return clust

def find_neighbours( x, y, part_number ) :
    global image

    if x < 0 or y < 0 or x >= width or y >= height :
        return
    else :
        if image[ x ][ y ] == 1 :
            image[ x ][ y ] = part_number
            find_neighbours( x + 1, y, part_number )
            find_neighbours( x + 1, y + 1, part_number )
            find_neighbours( x + 1, y - 1, part_number )

            find_neighbours( x - 1, y, part_number )
            find_neighbours( x - 1, y + 1, part_number )
            find_neighbours( x - 1, y - 1, part_number )

            find_neighbours( x, y, part_number )
            find_neighbours( x, y + 1, part_number )
            find_neighbours( x, y - 1, part_number )




# Array now contains all clusters
# numbered from 2 upward
writer=csv.writer(open("ellipse100thick.csv","wb"))
writer.writerow("SkyId,pred_x1,pred_y1,pred_x2,pred_y2,pred_x3, pred_y3".split(","))
for k in range(1,100):

    image_file = Image.open("../tempimages/thick/"+str(k)+".png")
    image_file = image_file.convert('1')
    pixels     = image_file.load()

    width  = image_file.size[0]
    height = image_file.size[1]

    image = [[ to_1( pixels[i,j] ) for i in range(width)] for j in range(height)]
    one_found = True
    part = 2
    while one_found :
        one_found = False
        for y in range(0,height):
            for x in range(0,width):
                if image[x][y] == 1:
                    find_neighbours(x,y,part)
                    part = part + 1
                    one_found = True
    clust=np.asarray(dump_pixels( image ))
    #print clust 
    clustno=stats.mode( clust[np.where(clust>0)])[0]
    #print clustno
    clustno2=np.where(clust==clustno)
    #print clustno2
    coords=[]
    for i in clustno2[0]:
        coords.append([i%width, i/width])
    coords=np.array(coords)
    #print coords
    ans=coords.mean(axis=0)
    #print ans
    anscaled=ans*4000/width
    #print anscaled
    #print anscaled[0],anscaled[1]
    #print anscaled[0],4000-anscaled[1]
    print "Processed sky"+str(k)
    writer.writerow(["Sky"+str(k),str(anscaled[0]),str(anscaled[1]),0,0,0,0])