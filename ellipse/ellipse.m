clear ; close all; clc
I=imread('../tempimages/201.png');
imshow(I);
pause;
I=imread('../tempimages/superimposed/201p.png');
%s  = regionprops(I, 'Centroid','PixelValues','BoundingBox');
BW = I <min(min(I))*1.03;
imshow(BW);

s = regionprops(BW,I,'Centroid','PixelValues','BoundingBox');
%cen=stats(23)
s.Centroid;
%imshow(I)
hold on
%centroids = cat(1, s.Centroid);
%[IDX,C,SUMD]=kmeans(centroids,1);
%SUMD
numObj = numel(s);
for k = 1 : numObj
    s(k).StandardDeviation = std(double(s(k).PixelValues));
    text(s(k).Centroid(1),s(k).Centroid(2), ...
        sprintf('%2.1f', s(k).StandardDeviation), ...
        'EdgeColor','b','Color','r');
end
hold off
pause;
sStd = [s.StandardDeviation];
lowStd = find(sStd < 1);

imshow(I);
title('Objects Having Standard Deviation < 50');
hold on;
for k = 1 : length(lowStd)
    rectangle('Position', s(lowStd(k)).BoundingBox, ...
        'EdgeColor','y');
end
hold off;
%plot(stats.Centroid(1),stats.Centroid(2), 'b*');
%hold off
%keyboard()
%BW = I >128;
%imshow(BW)
pause;
imshow(I)

hold on
s = regionprops(BW, I, {'Centroid','WeightedCentroid'});
numObj = numel(s);
for k = 1 : numObj
    plot(s(k).WeightedCentroid(1), s(k).WeightedCentroid(2), 'r*');
    plot(s(k).Centroid(1), s(k).Centroid(2), 'bo');
end
hold off