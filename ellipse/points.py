'''
Points algorithm
This is a image processing algorithm to find out halo centres
It does following things to a sky:
1.Draw ellpses' major axis with alpha
2.scale them to make them bigger
3.sky is 4200x4200 to get accuracy
4.save the figure

Next step is to crop the image according to lenstool estimates. extractsquare.py will do it.
'''
import csv
import pylab as p
from matplotlib.backends.backend_pdf import PdfPages
import math
from numpy import linspace
from scipy import pi,sin,cos
import gc
import Image, ImageDraw, aggdraw
import numpy as np
def ellipse(ra,rb,ang,x0,y0,Nb=50):
	'''ra - major axis length
 rb - minor axis length
 ang - angle
 x0,y0 - position of centre of ellipse
 Nb - No. of points that make an ellipse

 based on matlab code ellipse.m written by D.G. Long,
 Brigham Young University, based on the
 CIRCLES.m original
 written by Peter Blattner, Institute of Microtechnology,
 University of
 Neuchatel, Switzerland, blattner@imt.unine.ch
'''
	xpos,ypos=x0,y0
	radm,radn=ra,rb
	an=ang

	co,si=cos(an),sin(an)
	the=linspace(0,2*pi,Nb)
	X=radm*cos(the)*co-si*radn*sin(the)+xpos
	Y=radm*cos(the)*si+co*radn*sin(the)+ypos
	return X,Y

def test():
	#Read Halos
	halos=csv.reader(open("../Raw/Training_halos.csv","rb"))
	halos.next()
	#Read Gridded signal benchmark
	#halos2=csv.reader(open("../misc/lenstool.benchmark.csv","rb"))
	#halos2.next()
	#pp = PdfPages('train_gridded.pdf')
	#Read each sky in Training set
	for sky in range(41,81):
		f=csv.reader(open("../Raw/Test_Skies/Test_Sky"+str(sky)+".csv","rb"))
		#f=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(sky)+".csv","rb"))
		#f=csv.reader(open("../antihalo/processed/Train_Skies/Training_Sky"+str(sky)+".csv","rb"))
		#f=csv.reader(open("../antihalo/milestone/processed/Test_Skies/Test_Sky"+str(sky)+".csv","rb"))
		f.next()
		print "Plotting Sky"+str(sky)
		#fig = p.figure(figsize=(8,8))
		#p.axis([0,4200,0,4200]) #Plot size
		
		scale=5000 #Scale
		im=Image.new("L",(4200,4200),"#FFFFFF")
		#halo2=halos2.next()
		#lensx=float(halo2[1])
		#lensy=float(halo2[2])
		#print lensx,lensy
		d=aggdraw.Draw(im)
		pen = aggdraw.Pen("black", 700,opacity=1)
		for line in f:
			#im2=Image.new("L",(420,420),"#FFFFFF")
			#print line
			x=float(line[1])		
			y=float(line[2])
			e1=float(line[3])
			e2=float(line[4])
			#print x,y,math.sqrt((lensx-x)**2+(lensy-y)**2)
			#break
			#if math.sqrt((lensx-x)**2+(lensy-y)**2)>1500:
			#	continue
			#print line
			e=math.sqrt(e1**2+e2**2)
			a = 1/(1.-e)*scale
			b=1/(1.+e)*scale*0#*10
			theta=math.atan2(e2,e1)/2.+pi/2.
			#xd=(1-e1)*x-e2*y
			#yd=-e2*x+(1+e2)*y

			#print theta
			#draw.line((x/10.,y/10.,xd/10.,yd/10.),fill=128)
		    
			X,Y=ellipse(a,b,theta,x,y,Nb=3)
			#print np.array(X),np.array(Y)
			d.line((X[0],4200-Y[0],X[1],4200-Y[1]),pen)
			#p.plot(X,Y,color=(0,0,0,.1),marker=".",ms=1) # Plot galaxy
			
			#p.plot(1086.80,1114.61,"g+",ms=100)
			#p.plot(1091.4096+4,1252.7328-20,"b+",ms=100)
			#p.plot(1091.4096,1252.7328,"r+",ms=100)
		#Uncomment below if you want to pl1t halos
		#p.show()
		d.flush()
		#im.show()
		im.save("../tempimages/test/"+str(sky)+".png")
		
		
		#p.grid(False) #Don't show grid
		#ax = p.gca()
		#ax.yaxis.set_visible(False) #Don't show axes
		#ax.xaxis.set_visible(False)
		#p.tight_layout(pad=0) # No margin
		#p.savefig("../tempimages/"+str(sky)+".png")
		#p.clf()
		#p.close()

if __name__ == '__main__':
	test()