"""
Takes images from tempimages and superimposes them to create average.png which
is mean of all images inverted"""

from PIL import Image,ImageOps
import numpy as np
#ims=[]

ims=np.zeros([100,4200,4200])
for i in range(1,101):
	image_file = Image.open("../tempimages/"+str(i)+".png")
	print "Processed image",str(i)
	#image_file = image_file.convert()
	image_file=ImageOps.grayscale(image_file)
	imgarray=np.asarray(image_file)
	ims[i-1]=np.array([imgarray])
	#ims.append(imgarray)
	#print len(imgarray)
	
#ims=np.array(ims)
print np.shape(ims)
print ims
#imsmax=ims.max()
#imsmin=ims.min()
#imsmean=ims.mean()
ans=255-ims.mean(axis=0)
#ans=(imsmax-ims.mean(axis=0))*255/(imsmax-imsmin)
#ans=((ans.max()-ans)*255)/(ans.max()-ans.min())
#print ims.mean(axis=0)
#print np.shape(ans),ans.mean(),ans.min(),ans.max()

output=Image.fromarray(ans)
output=output.convert("L")
#output.show()
output.save("averagetemp.png")