"""Takes results from matlab knn algorithm and formats it for submission"""

import csv,subprocess
ass=[]
with open("Results/2trpo1.csv","rb") as f:
    with open("Results/2trpo2.csv","rb") as g:
	    reader=csv.reader(f)
	    reader2=csv.reader(g)
	    #reader2.next()
	    for i in reader:
	        temp=["Sky"+str(i[0])]+i[1:]+reader2.next()[1:3]+[0,0]
	        ass.append(temp)
#print ass[:2]
of="Results/2trpoa2.csv"
with open(of,"wb") as f:
    writer=csv.writer(f)
    writer.writerow("SkyId,pred_x1,pred_y1,pred_x2,pred_y2,pred_x3,pred_y3".split(","))
    for i in ass:
        writer.writerow(i)
subprocess.call(["python", "../tools/DarkWorldsMetric.py", of, "../tools/2Training_halos.csv"]) 
subprocess.call(["python", "../tools/DarkWorldsMetric.py", "../lenstool/Results/2trlt.csv", "../tools/2Training_halos.csv"]) 
