clear ; close all; clc
%I=imread('../tempimages/138.png');
%imshow(I);
%pause;

%I=imread('../tempimages/144.png');
files=[41,80];
ansa=zeros(files(2)-files(1)+1,7);
for A=files(1):files(2)
    A
    file=strcat('../tempimages/test/superimposed/',int2str(A),'p.png');
    %file=strcat('../tempimages/superimposed/',int2str(A),'pa2.png');
    I=imread(file);
    imshow(I);
    %pause;
    BW = I <=min(min(I))*1.04;
    imshow(BW);
    %pause

    hold on
    s = regionprops(BW, I, {'Centroid','WeightedCentroid'});
    numObj = numel(s);
for k = 1 : numObj
   plot(s(k).WeightedCentroid(1), s(k).WeightedCentroid(2), 'r*');
    %plot(s(k).Centroid(1), s(k).Centroid(2), 'bo');
end

    centroids=zeros(numObj,2);
    for k = 1 : numObj
        centroids(k,:)=[s(k).Centroid(1), s(k).Centroid(2)];
    end

    [IDX,C,SUMD]=kmeans(centroids,2);
    C;
for i=1:length(C(:,1))
    plot(C(i,1),C(i,2),'gs','LineWidth',2,'MarkerEdgeColor','k','MarkerFaceColor','g','MarkerSize',10);
end
    
    C(1,2)=4200-C(1,2);
    C(2,2)=4200-C(2,2);
    %C(3,2)=4200-C(3,2);
    C=[C;0 0];
    
    ansa(A-files(1)+1,1)=A;
    %ansa(A-files(1)+1,2:end)=C;
    ansa(A-files(1)+1,2:end)=reshape(C.',1,[]);
    C;
hold off
%pause;
end
csvwrite('Results/2tepo.csv',ansa)