# vi: spell spl=en
#
#
library(ggplot2)
set.seed(1990)
rm(list=objects())

halo_count <- read.table( 
    '../Raw/Training_halos.csv',
    header=TRUE, sep=',' )
gridded <- read.table( 
  '../Gridded_Signal/Results/2trgs.csv', header=TRUE, sep=',' )
gridded$kind <- rep( 'gridded', n=100)
gridded$count <- 2

maxlike <- read.table( 
  '../maximumLikelihood/Results/2trml.csv', header=TRUE, sep=',' )
maxlike2 <- read.table( 
  '../maximumLikelihood/Results/2trml2.csv', header=TRUE, sep=',' )
maxlike$kind <- rep( 'maxlike', n=100)
maxlike$count <-2
maxlike$pred_x2<-maxlike2$pred_x1
maxlike$pred_y2<-maxlike2$pred_y1


points <- read.table( 
  '../ellipse/Results/2trpo.csv', header=TRUE, sep=',' )
points$kind <- rep( 'points', n=100)
points$count <- 2
names(points) <- names(gridded)

poa <- read.table( 
  '../ellipse/Results/2trpoa2.csv', header=TRUE, sep=',' )
poa$kind <- rep( 'poa', n=100)
poa$count <- 2
names(poa) <- names(gridded)

antihalo <- read.table( 
  '../ellipse/Results/2trpoa2.csv', header=TRUE, sep=',' )
antihalo$kind <- rep( 'antihalo', n=100)
antihalo$count <- 2
names(points) <- names(gridded)

lens   <- read.table(
  '../lenstool/Results/2trlt2.csv', header=TRUE, sep=',' )
lens$kind <- rep( 'lens', n=100 )
lens$count <- 2
names(lens) <- names(gridded)


lta1   <- read.table(
  '../lenstool/Results/2trlta1halof.csv', header=TRUE, sep=',' )
lta1$kind <- rep( 'lta1', n=100 )
lta1$count <- 2
#names(lta1) <- names(gridded)

lta   <- read.table(
  '../lenstool/Results/2trlta.csv', header=TRUE, sep=',' )
lta$kind <- rep( 'lta', n=100 )
lta$count <- 2
lta$pred_x2<-lta1$pred_x1
lta$pred_y2<-lta1$pred_y1
lta$pred_x3<-0
lta$pred_y3<-0
#names(lta) <- names(gridded)
lta2   <- read.table(
  '../tools/temp1.csv', header=TRUE, sep=',' )
lta2$kind <- rep( 'lta2', n=100 )
lta2$count <- 2


rf2   <- read.table(
  '../learningModels/Results/2trrf2.csv', header=TRUE, sep=',' )
rf2$kind <- rep( 'rf2', n=100 )
rf2$count <- 2
names(rf2) <- names(gridded)

rf1   <- read.table(
  '../learningModels/Results/2trrf.csv', header=TRUE, sep=',' )
rf1$kind <- rep( 'rf1', n=100 )
rf1$count <- 2
rf1$pred_x2<-rf2$pred_x1
rf1$pred_y2<-rf2$pred_y1
names(rf1) <- names(gridded)

#lens$pred_x2<-lens$pred_x1
#lens$pred_y2<-lens$pred_y1
combined <- rbind(lens,maxlike,lta,lta2,poa,points)


two_halo <- combined[ combined$count == 2, ]
for ( skydata in split( two_halo, f=two_halo$SkyId ) ) {
  if ( nrow( skydata ) > 0 ) {
    x <- c( skydata$pred_x1, skydata$pred_x2 )
    y <- c( skydata$pred_y1, skydata$pred_y2 )
    pp <- data.frame(x,y)
    pp <- pp[ pp$x > 0, ]
    cl <- kmeans(pp,2)
    lx<-lens[ lens$SkyId == skydata$SkyId, 'pred_x1']
    ly<-lens[ lens$SkyId == skydata$SkyId, 'pred_y1']
    x1<-cl$centers[1,'x']
    y1<-cl$centers[1,'y']
    x2<-cl$centers[2,'x']
    y2<-cl$centers[2,'y']
    d1<-sqrt((x1-lx)**2+(y1-ly)**2)
    d2<-sqrt((x2-lx)**2+(y2-ly)**2)
    if (d1>d2) {
    lens[ lens$SkyId == skydata$SkyId, 'pred_x2'] <- cl$centers[1,'x']
    lens[ lens$SkyId == skydata$SkyId, 'pred_y2'] <- cl$centers[1,'y']
    } else{lens[ lens$SkyId == skydata$SkyId, 'pred_x2'] <- cl$centers[2,'x']
    lens[ lens$SkyId == skydata$SkyId, 'pred_y2'] <- cl$centers[2,'y']
  }
  }
}
lens$kind <- NULL
lens$count <- NULL
#lens<-lens[101:200,]
write.table( lens, 
             'Results/2trnlp.csv', row.names=FALSE, 
             col.names=TRUE, sep=',', quote=FALSE )

system("python ../tools/DarkWorldsMetric.py Results/2trnlp.csv ../tools/2")
#system("python ../tools/DarkWorldsMetric.py ../lenstool/Results/2trlt.csv ../tools/2")
