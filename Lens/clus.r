# vi: spell spl=en
#
library(ggplot2)
library(reshape2)

rm(list=objects())

# What kind of predictions are we clustering
# (train or test )
kind <- 'train'    # Default

args <- commandArgs( TRUE )
if ( length( args ) > 0 ) {
    kind  <- args[1]
}

#-------------------------------------------------------------------------

if ( kind == 'train' ) {
    halo_count <- read.table( 
        '../Raw/Training_halos.csv',
        header=TRUE, sep=',' )
    gridded <- read.table( 
      '../Gridded_Signal/Results/trgs.csv', 
      header=TRUE, sep=',' )
    gridded$kind <- rep( 'gridded', n=300)
    gridded$count <- halo_count$numberHalos

    maxlike <- read.table( 
      '../maximumLikelihood/Results/trml.csv',
      header=TRUE, sep=',' )
    maxlike$kind <- rep( 'maxlike', n=300)
    maxlike$count <- halo_count$numberHalos

    # Can't call it points (cause there is a function called points)
#    algpoints <- read.table( 
#      '../ellipse/Results/2trpoaf.csv', 
#      header=TRUE, sep=',' )
#    algpoints$kind <- rep( 'points', n=100)
#    algpoints$count <- 2
#    names(algpoints) <- names(gridded)
# --- not the right format

    lens   <- read.table(
      '../lenstool/Results/trlt.csv', header=TRUE, sep=',' )
    lens$kind <- rep( 'lens', n=300 )
    lens$count <- halo_count$numberHalos
    names(lens) <- names(gridded)

    combined <- rbind( lens, gridded, maxlike )  # , algpoints )
} else {
    halo_count <- read.table( 
        '../Raw/Test_haloCounts.csv',
        header=TRUE, sep=',' )

    gridded <- read.table( 
        '../Gridded_Signal/Results/tegs.csv',
        header=TRUE, sep=',' )
    gridded$kind <- rep( 'gridded', n=120)
    gridded$count <- halo_count$NumberHalos

    maxlike <- read.table( 
        '../maximumLikelihood/Results/teml.csv', 
        header=TRUE, sep=',' )
    maxlike$kind <- rep( 'maxlike', n=120)
    maxlike$count <- halo_count$NumberHalos

    algpoints <- read.table( 
      '../ellipse/Results/2tepo.csv', 
      header=TRUE, sep=',' )
    algpoints$kind <- rep( 'points', n=40)
    algpoints$count <- 2
    names(algpoints) <- names(gridded)

    lens   <- read.table(
        '../lenstool/Results/telt.csv', 
        header=TRUE, sep=',' )
    lens$kind <- rep( 'lens', n=120 )
    lens$count <- halo_count$NumberHalos
    names(lens) <- names(gridded)

    combined <- rbind( lens, gridded, maxlike, algpoints )
}

# Actual clustering algorithm

pcluster <- function( lens ) {
    two_halo <- combined[ combined$count == 2, ]
    for ( skydata in split( two_halo, f=two_halo$SkyId ) ) {
        if ( nrow( skydata ) > 0 ) {
            x <- c( skydata$pred_x1, skydata$pred_x2 )
            y <- c( skydata$pred_y1, skydata$pred_y2 )
            pp <- data.frame( x, y )
            # Skip the ones that are not a prediction
            pp <- pp[ pp$x > 0, ]
            cl <- kmeans(pp,2)
            # Assumption, first halo is the big one....
            # and is predicted correctly by the lenstool
            lens[ lens$SkyId == skydata$SkyId, 'pred_x1'] <- cl$centers[1,'x']
            lens[ lens$SkyId == skydata$SkyId, 'pred_y1'] <- cl$centers[1,'y']
            lens[ lens$SkyId == skydata$SkyId, 'pred_x2'] <- 
                            cl$centers[2,'x']
            lens[ lens$SkyId == skydata$SkyId, 'pred_y2'] <-
                            cl$centers[2,'y']
        }
    }
    return( lens )
}

org_lens <- lens
new_lens <- pcluster( lens )
# Can't summit these
new_lens$kind <- NULL
new_lens$count <- NULL

if ( kind == 'test' ) {
    filename <- 'Results/cluster_test.csv'
} else {
    filename <- 'Results/cluster_train.csv'
}

write.table( new_lens, file=filename,
    row.names=FALSE, 
    col.names=TRUE, sep=',', quote=FALSE )


#-----------------------------------------
# Variance analysis

multi_run <- data.frame()
for ( i in 1:30 ) {
    lens <- org_lens
    new_lens <- pcluster( lens )
    two_halo <- new_lens[ combined$count == 2, ]
    multi_run <- rbind( multi_run, two_halo[ 1:20, ] )
}
multi_run$kind <- NULL
multi_run$count <- NULL

print( multi_run )

print( aggregate( multi_run$pred_x2, by=list(multi_run$SkyId), sd ) )

m_multi_run <- melt( multi_run )

p <- ggplot( m_multi_run, aes(x=SkyId, y=value ) ) + 
        geom_boxplot() + geom_jitter(size=1) + 
        stat_summary(fun.data = "mean_cl_boot", colour = "red") +
        facet_wrap(~variable)

