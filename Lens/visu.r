# vi: spell spl=en
#

library(ggplot2)

rm(list=objects())

pred_halo <- read.table( 'lenstooltraining1halo.csv', header=TRUE, sep=',' )
real_halo <- read.table( 'Training_halos.csv', header=TRUE, sep=',' )
comb_halo <- merge( real_halo, pred_halo )

comb_halo$dx <- comb_halo$halo_x1 - comb_halo$pred_x1
comb_halo$dy <- comb_halo$halo_y1 - comb_halo$pred_y1


p <- ggplot(comb_halo,aes(x=dx,y=dy))+geom_point()

