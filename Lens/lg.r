# vi: spell spl=en
#
rm(list=objects())
pred_halo <- read.table( 'lenstooltraining1halo.csv', header=TRUE, sep=',' )
real_halo <- read.table( 'Training_halos.csv', header=TRUE, sep=',' )
comb_halo <- merge( real_halo, pred_halo )

# Outlier removal.  (Does not improve score)
# comb_halo <- comb_halo[-c(66,81,6),]

# Model for the real halo position based on the by the lenstool 
# predicted positions
model_x <- lm(halo_x1~pred_x1,comb_halo)
model_y <- lm(halo_y1~pred_y1,comb_halo)

# Make predictions


# Predictions for the test skies
lens <- read.table( 'lenstool.benchmark.csv', header=TRUE, sep=',' )
lens_org <- lens

#-----------------------------------------------------------------------
# The first 40 skies have one halo
#lens_1 <- lens[1:40,]

new_x <- predict.lm( model_x, data.frame( pred_x1=lens_1$halo_x1 ) )
new_y <- predict.lm( model_y, data.frame( pred_y1=lens_1$halo_y1 ) )
lens[1:40,'halo_x1'] <- new_x
lens[1:40,'halo_y1'] <- new_y

write.table( lens, file="new_test.csv", row.names=FALSE, col.names=TRUE,
   sep=',', quote=FALSE )

#-----------------------------------------------------------------------
# Correct training results
new_x <- predict.lm( model_x, data.frame( pred_x1=pred_halo$pred_x1 ) )
new_y <- predict.lm( model_y, data.frame( pred_y1=pred_halo$pred_y1 ) )
pred_halo$pred_x1 <- new_x
pred_halo$pred_y1 <- new_y

write.table( pred_halo, file="new_train.csv", row.names=FALSE, col.names=TRUE,
   sep=',', quote=FALSE )


