# vi: spell spl=en
#
#
library(ggplot2)
set.seed(1990)
rm(list=objects())

halo_count <- read.table( 
    '../Raw/Test_haloCounts.csv',
    header=TRUE, sep=',' )


maxlike <- read.table( 
  '../maximumLikelihood/Results/submission/2teml_temla.csv', header=TRUE, sep=',' )

maxlike$kind <- rep( 'maxlike', n=40)
maxlike$count <-2

gridded <- read.table( 
  '../Gridded_Signal/Results/2tegs.csv', header=TRUE, sep=',' )

gridded$kind <- rep( 'gridded', n=40)
gridded$count <-2


points <- read.table( 
  '../ellipse/Results/2tepo.csv', header=TRUE, sep=',' )
points$kind <- rep( 'points', n=40)
points$count <- 2
names(points) <- names(gridded)
points$SkyId<-paste("Sky",points$SkyId,sep="")
#poa <- read.table( 
#  '../ellipse/Results/2tepoa.csv', header=TRUE, sep=',' )
#poa$kind <- rep( 'poa', n=100)
#poa$count <- 2
#names(poa) <- names(gridded)

antihalo <- read.table( 
  '../ellipse/Results/2tepoa.csv', header=TRUE, sep=',' )
antihalo$kind <- rep( 'antihalo', n=40)
antihalo$count <- 2

names(antihalo) <- names(gridded)

antihalo$SkyId<-paste("Sky",antihalo$SkyId,sep="")

lens   <- read.table(
  '../lenstool/Results/2telt.csv', header=TRUE, sep=',' )
lens$kind <- rep( 'lens', n=40 )
lens$count <- 2
names(lens) <- names(gridded)

rf   <- read.table(
  '../learningModels/Results/submission/2terf_terfa.csv', header=TRUE, sep=',' )
rf$kind <- rep( 'rf1', n=40 )
rf$count <- 2
names(rf) <- names(gridded)

#lens$pred_x2<-lens$pred_x1
#lens$pred_y2<-lens$pred_y1
combined <- rbind(lens,maxlike,points,antihalo,rf)


two_halo <- combined[ combined$count == 2, ]
for ( skydata in split( two_halo, f=two_halo$SkyId ) ) {
  if ( nrow( skydata ) > 0 ) {
    x <- c( skydata$pred_x1, skydata$pred_x2 )
    y <- c( skydata$pred_y1, skydata$pred_y2 )
    pp <- data.frame(x,y)
    pp <- pp[ pp$x > 0, ]
    cl <- kmeans(pp,2)
    lx<-lens[ lens$SkyId == skydata$SkyId, 'pred_x1']
    ly<-lens[ lens$SkyId == skydata$SkyId, 'pred_y1']
    x1<-cl$centers[1,'x']
    y1<-cl$centers[1,'y']
    x2<-cl$centers[2,'x']
    y2<-cl$centers[2,'y']
    d1<-sqrt((x1-lx)**2+(y1-ly)**2)
    d2<-sqrt((x2-lx)**2+(y2-ly)**2)
    if (d1>d2) {
    lens[ lens$SkyId == skydata$SkyId, 'pred_x2'] <- cl$centers[1,'x']
    lens[ lens$SkyId == skydata$SkyId, 'pred_y2'] <- cl$centers[1,'y']
    } else{lens[ lens$SkyId == skydata$SkyId, 'pred_x2'] <- cl$centers[2,'x']
    lens[ lens$SkyId == skydata$SkyId, 'pred_y2'] <- cl$centers[2,'y']
  }
  }
}
lens$kind <- NULL
lens$count <- NULL
#lens<-lens[101:200,]
write.table( lens, 
             'Results/2tenlp.csv', row.names=FALSE, 
             col.names=TRUE, sep=',', quote=FALSE )

#system("python ../tools/DarkWorldsMetric.py Results/2trnlp.csv ../tools/2")
#system("python ../tools/DarkWorldsMetric.py ../lenstool/Results/2trlt.csv ../tools/2")
