'''Creates pdf of galaxies and halos
Green circle is ground truth
Red circle is solution from Gridded_Signal_benchmark
'''
import csv
import pylab as p
from matplotlib.backends.backend_pdf import PdfPages
import math
from numpy import linspace
from scipy import pi,sin,cos


def ellipse(ra,rb,ang,x0,y0,Nb=50):
	'''ra - major axis length
 rb - minor axis length
 ang - angle
 x0,y0 - position of centre of ellipse
 Nb - No. of points that make an ellipse
'''
	xpos,ypos=x0,y0
	radm,radn=ra,rb
	an=ang

	co,si=cos(an),sin(an)
	the=linspace(0,2*pi,Nb)
	X=radm*cos(the)*co-si*radn*sin(the)+xpos
	Y=radm*cos(the)*si+co*radn*sin(the)+ypos
	return X,Y
	p.hold(False) #Fix memory leak in matplotlib
def test():
	gts=csv.reader(open("../tools/1Training_halos.csv","rb"))
	gts.next()
	gss=csv.reader(open("../Gridded_Signal/Results/trgs.csv","rb"))
	gss.next()
	lts=csv.reader(open("../lenstool/Results/trlt.csv","rb"))
	lts.next()
	mls=csv.reader(open("../maximumLikelihood/Results/trml.csv","rb"))
	mls.next()
	#pos=csv.reader(open("../ellipse/Results/trpof.csv","rb"))
	#pos.next()
	pp = PdfPages('../plots/Training1halos.pdf')
	for sky in xrange(1,101):
		f=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(sky)+".csv","rb"))
		f.next()
		scale=50
		gt=gts.next()
		gs=gss.next()
		ml=mls.next()
		lt=lts.next()
		#print lt,nl

		assert gt[0]==gs[0]==lt[0]==ml[0]
		print "Plotting Sky"+str(sky)
		fig = p.figure(figsize=(8,8))
		p.axis([0,4200,0,4200])
		for line in f:
			x=float(line[1])		
			y=float(line[2])
			e1=float(line[3])
			e2=float(line[4])

			e=math.sqrt(e1**2+e2**2)
			a = 1/(1.-e)*scale
			b=1/(1.+e)*scale*0
			theta=math.atan2(e2,e1)/2.
			X,Y=ellipse(a,b,theta,x,y,Nb=3)
			p.plot(X,Y,color="#DDDDDD",marker=".",ms=.2) # black ellipse
		MARKERSIZE=1
		MARKERTHICKNESS=1
		ALHPA=1
		HALOSIZE=150
		X,Y=ellipse(HALOSIZE,HALOSIZE,3.1416/2,float(gt[4]),float(gt[5])) # Ground truth
		p.plot(X,Y,color="#00FFFF", marker=".",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # cyan
		X,Y=ellipse(HALOSIZE,HALOSIZE,3.1416/2,float(lt[1]),float(lt[2])) #lenstool
		p.plot(X,Y,color="#0033FF", marker=".",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # blue
		X,Y=ellipse(HALOSIZE,HALOSIZE,3.1416/2,float(gs[1]),float(gs[2])) #lenstool
		p.plot(X,Y,color="#33A02C", marker=".",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # blue
		X,Y=ellipse(HALOSIZE,HALOSIZE,3.1416/2,float(ml[1]),float(ml[2])) #lenstool
		p.plot(X,Y,color="#FDBF6F", marker=".",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # blue
		p.grid(True)
		p.savefig(pp, format='pdf')
		p.clf()
	pp.close()

if __name__ == '__main__':
	test()	