import csv

file1="../learningModels/Results/2terf.csv"
file2="../learningModels/Results/2terfa.csv"
#file2="../learningModels/Results/2trrf2.csv"
of="../learningModels/Results/submission/2terf_terfa.csv.csv"
ans=[]
with open(file1,"rb") as f:
	with open(file2,"rb") as g:
		r1=csv.reader(f)
		r2=csv.reader(g)
		ans.append(r1.next())
		r2.next()
		for i in r1:
			ans.append(i[0:3]+r2.next()[1:3]+[0,0])
with open(of,"wb") as f:
	writer=csv.writer(f)
	writer.writerows(ans)