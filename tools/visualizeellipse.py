'''Creates pdf of galaxies and halos
Green circle is ground truth
Red circle is solution from Gridded_Signal_benchmark
'''
import csv
import pylab as p
from matplotlib.backends.backend_pdf import PdfPages
import math
from numpy import linspace
from scipy import pi,sin,cos


def ellipse(ra,rb,ang,x0,y0,Nb=50):
	'''ra - major axis length
 rb - minor axis length
 ang - angle
 x0,y0 - position of centre of ellipse
 Nb - No. of points that make an ellipse

 based on matlab code ellipse.m written by D.G. Long,
 Brigham Young University, based on the
 CIRCLES.m original
 written by Peter Blattner, Institute of Microtechnology,
 University of
 Neuchatel, Switzerland, blattner@imt.unine.ch
'''
	xpos,ypos=x0,y0
	radm,radn=ra,rb
	an=ang

	co,si=cos(an),sin(an)
	the=linspace(0,2*pi,Nb)
	X=radm*cos(the)*co-si*radn*sin(the)+xpos
	Y=radm*cos(the)*si+co*radn*sin(the)+ypos
	return X,Y
	p.hold(False) #Fix memory leak in matplotlib
def test():
	halos=csv.reader(open("../Raw/Training_halos.csv","rb"))
	halos.next()
	halos2=csv.reader(open("../Gridded_Signal/Gridded_Signal_benchmark.csv","rb"))
	halos2.next()

	halos3=csv.reader(open("../ellipse/ellipse100thick.csv","rb"))
	halos3.next()
	pp = PdfPages('../plots/visualizeellpise.pdf')
	for sky in range(1,20):
		f=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(sky)+".csv","rb"))
		f.next()
		print "Plotting Sky"+str(sky)
		fig = p.figure(figsize=(1,1))
		p.axis([0,4000,0,4000])
		
		scale=300
		
		for line in f:
			#print line
			x=float(line[1])		
			y=float(line[2])
			e1=float(line[3])
			e2=float(line[4])

			e=math.sqrt(e1**2+e2**2)
			a = 1/(1.-e)*scale
			b=1/(1.+e)*scale
			theta=math.atan2(e2,e1)/2.
			#print theta
			X,Y=ellipse(a,b,theta,x,y,Nb=10)
			p.plot(X,Y,"k.-",ms=1) # black ellipse
		halo=halos.next()
		X,Y=ellipse(200,200,3.1416/2,float(halo[4]),float(halo[5]))
		p.plot(X,Y,"g.-",ms=2) # green ellhalopse
		halo2=halos2.next()
		X,Y=ellipse(200,200,3.1416/2,float(halo2[1]),float(halo2[2]))
		p.plot(X,Y,"r.-",ms=2) # red ellhalopse
		halo3=halos3.next()
		X,Y=ellipse(200,200,3.1416/2,float(halo3[1]),float(halo3[2]))
		p.plot(X,Y,"m.-",ms=2) # magenta ellhalopse
		if float(halo[6])!=0:

			X,Y=ellipse(200,200,3.1416/2,float(halo[6]),float(halo[7]))
			p.plot(X,Y,"g.-",ms=2) # green ellipse
			X,Y=ellipse(200,200,3.1416/2,float(halo2[3]),float(halo2[4]))
			p.plot(X,Y,"r.-",ms=2) # red ellhalopse
			X,Y=ellipse(200,200,3.1416/2,float(halo3[3]),float(halo3[4]))
			p.plot(X,Y,"m.-",ms=2) # magenta ellhalopse

		if float(halo[8])!=0:
			X,Y=ellipse(200,200,3.1416/2,float(halo[8]),float(halo[9]))
			p.plot(X,Y,"g.-",ms=2) # green ellipse
			X,Y=ellipse(200,200,3.1416/2,float(halo2[5]),float(halo2[6]))
			p.plot(X,Y,"r.-",ms=2) # red ellhalopse
			X,Y=ellipse(200,200,3.1416/2,float(halo3[5]),float(halo3[6]))
			p.plot(X,Y,"m.-",ms=2) # magenta ellhalopse

		
		#break

		p.grid(True)
		ax = p.gca()
		ax.yaxis.set_visible(False)
		ax.xaxis.set_visible(False)
		p.tight_layout(pad=0)
		p.savefig(pp, format='pdf')
		p.clf()
	pp.close()

if __name__ == '__main__':
	test()