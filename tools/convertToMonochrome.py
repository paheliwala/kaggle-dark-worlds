'''Converts images in tempimages/ to monochrome using imagemagick
saves it to tempimages/mono'''
from subprocess import call
import shlex
for i in range(1,10):
	args=shlex.split("convert ../tempimages/"+str(i)+".png -monochrome  ../tempimages/mono/"+str(i)+".png ")
	call(args)