'''Extract features for application of ML algorithm'''
from time import time
import csv
import pylab as p
from matplotlib.backends.backend_pdf import PdfPages
import math
import numpy as np
from numpy import linspace
from scipy import pi,sin,cos
a=time()
def ellipse(ra,rb,ang,x0,y0,Nb=50):
	'''ra - major axis length
 rb - minor axis length
 ang - angle
 x0,y0 - position of centre of ellipse
 Nb - No. of points that make an ellipse
'''
	xpos,ypos=x0,y0
	radm,radn=ra,rb
	an=ang

	co,si=cos(an),sin(an)
	the=linspace(0,2*pi,Nb)
	X=radm*cos(the)*co-si*radn*sin(the)+xpos
	Y=radm*cos(the)*si+co*radn*sin(the)+ypos
	return X,Y



halos=csv.reader(open("../lenstool/Results/2telt.csv","rb"))
halos.next()
fig = p.figure(figsize=(8,8))
p.axis([0,4000,0,4000])
for sky in range(41,81):
	processed=[]
	#galaxies_file=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(sky)+".csv","rb"))
	#galaxies_file=csv.reader(open("../Raw/Test_Skies/Test_Sky"+str(sky)+".csv","rb"))
	galaxies_file=csv.reader(open("../antihalo/milestone/processed/Test_Skies/Test_Sky"+str(sky)+".csv","rb"))
	#galaxies_file=csv.reader(open("../antihalo/processed/Train_Skies/Training_Sky"+str(sky)+".csv","rb"))
	galaxies_file.next()
	galaxies=np.array([i for i in galaxies_file])
	halo=halos.next()
	print halo
	halox=float(halo[1])
#	halox=float(halo[4])
	haloy=float(halo[2])
#	haloy=float(halo[5])

	#X,Y=ellipse(200,200,3.1416/2,float(halo[4]),float(halo[5]))
	#p.plot(X,Y,"g.-",ms=2) # green ellhalopse
	#scale=50
	for galaxy in galaxies:
		x=float(galaxy[1])		
		y=float(galaxy[2])
		e1=float(galaxy[3])
		e2=float(galaxy[4])
		#print x,y,e1,e2
		dist=math.sqrt((x-halox)**2+(y-haloy)**2)
		
		e=math.sqrt(e1**2+e2**2)
		a = 1/(1.-e)
		b=1/(1.+e)
		theta=math.atan2(e2,e1)/2.
		#print theta
		distgalaxies=np.array([math.sqrt((x-float(gi[1]))**2+(y-float(gi[2]))**2) for gi in galaxies])
		distgalaxies=np.delete(distgalaxies,np.where(distgalaxies==0.0))
		nearhalo=int(dist<600)
		nearestgalaxy1=distgalaxies.min()
		nearestgalaxy2=distgalaxies[distgalaxies>nearestgalaxy1].min()
		nearestgalaxy3=distgalaxies[distgalaxies>nearestgalaxy2].min()
		nearbygalaxies=len(distgalaxies[distgalaxies<300])
		ne1s=[]
		ne2s=[]
		nes=[]
		thetas=[]
		if nearbygalaxies<1:
			de1=-1
			stde1=-1
			de2=-1
			stde2=-1
			de=-1
			stde=-1
			dtheta=-1
			stdtheta=-1
		else:

			for ng in galaxies[distgalaxies<300]:
				#print ng
				#ntheta=math.atan2(float(ng[3]),float(ng[4]))/2.
				#nthetas.append(ntheta)
				ne1s.append(float(ng[3]))
				ne2s.append(float(ng[4]))
				thetas.append(math.atan2(float(ng[4]),float(ng[3]))/2.)
				nes.append(math.sqrt(float(ng[3])**2+float(ng[4])**2))
			ne1s=np.array(ne1s)
			ne2s=np.array(ne2s)
			nes=np.array(nes)
			thetas=np.array(thetas)
			#print ne1s
			#print ne1s.mean()
			#print e1
			de1=np.abs(ne1s.mean()-e1)
			stde1=ne1s.std()
			de2=np.abs(ne2s.mean()-e2)
			stde2=ne2s.std()
			de=np.abs(nes.mean()-e)
			stde=nes.std()
			dtheta=np.abs(thetas.mean()-theta)
			stdtheta=thetas.std()


		#dtheta=np.array(galaxies[distgalaxies<100][4]).mean()-theta
		#stdtheta=np.array(galaxies[distgalaxies<100][4]).std()
		#print x,y,e1,e2

		galaxy=[sky,nearhalo,x,y,e1,e2,e,b,a,a/b,de1,stde1,de2,stde2,nearbygalaxies]
		processed.append(galaxy)
		#X,Y=ellipse(a*scale,b*scale,theta,x,y,Nb=20)
		#if nearhalo:
		#	p.plot(X,Y,"r.-",ms=1)
		#else:
		#	p.plot(X,Y,"k.-",ms=1) 
		# black ellipse
	
	writer=csv.writer(open("../learningModels/processed/test/sky"+str(sky)+"a.csv","wb"))
	print "processed sky"+str(sky),time()-a
	writer.writerow(["Sky","nearhalo","x","y","e1","e2","e","b","a","a/b","de1","stde1","de2","stde2","nearbygalaxies"])
	writer.writerows(processed)
#p.grid(True)
#p.show()