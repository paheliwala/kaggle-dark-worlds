'''Creates pdf of galaxies and halos
Green circle is ground truth
Red circle is solution from Gridded_Signal_benchmark
'''
import csv
import pylab as p
from matplotlib.backends.backend_pdf import PdfPages
import math
from numpy import linspace
from scipy import pi,sin,cos


def ellipse(ra,rb,ang,x0,y0,Nb=50):
	'''ra - major axis length
 rb - minor axis length
 ang - angle
 x0,y0 - position of centre of ellipse
 Nb - No. of points that make an ellipse
'''
	xpos,ypos=x0,y0
	radm,radn=ra,rb
	an=ang

	co,si=cos(an),sin(an)
	the=linspace(0,2*pi,Nb)
	X=radm*cos(the)*co-si*radn*sin(the)+xpos
	Y=radm*cos(the)*si+co*radn*sin(the)+ypos
	return X,Y
	p.hold(False) #Fix memory leak in matplotlib
def test():
	halos=csv.reader(open("../Raw/Training_halos.csv","rb"))
	halos.next()
	halos2=csv.reader(open("../Gridded_Signal/Gridded_Signal_benchmark.csv","rb"))
	halos2.next()
	halos3=csv.reader(open("../ellipse/ellipse100thick.csv","rb"))
	halos3.next()
	halos4=csv.reader(open("../lenstool/lenstooltraining.csv","rb"))
	halos5=csv.reader(open("../learningModels/rfpredstraining.csv","rb"))
	halos5.next()
	halos6=csv.reader(open("../maximumLikelihood/Maximum_likelihood_Train.csv","rb"))
	halos6.next()
	halos7=csv.reader(open("../Circles/predict.csv","rb"))
	halos7.next()
	ensx=[ 3031.11684449, 530.41245897,4197.59742404,2146.75862253,3271.50873616
,1843.87626507,1268.01383532,3540.52124288,1656.82684823,3536.77015759
, 581.36883767, 694.24947751,2972.46814047,2071.92873302,3588.97656813
,3605.97934559,3407.3245964,372.7769919, 3074.92972569,3482.27873576]
	ensy=[403.86278567,1187.03246801,2031.73866675,2312.46035724,3722.38415299
,2601.1384965, 2943.05621739,1815.87218167, 441.90395522,2235.04969403
,2221.42257902,3142.53150406,3305.86256002,2237.58468682,3589.9023652
,1630.46645977,3107.27520709, 834.99201772,3317.31694507,2617.32092787]
	corrections=csv.reader(open("../ellipse/corrections.csv","rb"))
	corrections.next()
	pp = PdfPages('../plots/corrections.pdf')
	for sky in range(1,5):
		f=csv.reader(open("../Raw/Train_Skies/Training_Sky"+str(sky)+".csv","rb"))
		f.next()
		print "Plotting Sky"+str(sky)
		fig = p.figure(figsize=(8,8))
		p.axis([0,4200,0,4200])
		
		scale=50000
		
		for line in f:
			#print line
			x=float(line[1])		
			y=float(line[2])
			e1=float(line[3])
			e2=float(line[4])

			e=math.sqrt(e1**2+e2**2)
			a = 1/(1.-e)*scale*0
			b=1/(1.+e)*scale
			theta=math.atan2(e2,e1)/2.
			#print theta
			X,Y=ellipse(a,b,theta,x,y,Nb=20)
			p.plot(X,Y,color=(0,0,0,.1),marker=".",ms=1) # black ellipse
		halo=halos.next()
		MARKERSIZE=20
		MARKERTHICKNESS=2
		ALHPA=0.6
		X,Y=(float(halo[4]),float(halo[5])) # Ground truth
		p.plot(X,Y,"c+",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # cyan
		halo2=halos2.next()
		X,Y=(float(halo2[1]),float(halo2[2])) # Grid benchmark
		p.plot(X,Y,"g+",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # green
		#halo3=halos3.next()
		#X,Y=ellipse(200,200,3.1416/2,float(halo3[1]),float(halo3[2])) # thick ellipse
		#p.plot(X,Y,"r.-",ms=MARKERSIZE) # red
		halo4=halos4.next()
		X,Y=(float(halo4[0]),float(halo4[1])) #lenstool
		p.plot(X,Y,"b+",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # blue
		gridsize=500
		p.plot([X-gridsize,X+gridsize,X+gridsize,X-gridsize,X-gridsize],[Y+gridsize,Y+gridsize,Y-gridsize,Y-gridsize,Y+gridsize],"y+-") # blue
		halo5=halos5.next()
		X,Y=(float(halo5[1]),float(halo5[2])) #rf
		##p.plot(X,Y,"m+",ms=MARKERSIZE) # magenta 
		halo6=halos6.next()
		X,Y=(float(halo6[1]),float(halo6[2])) #maximum likelihood
		p.plot(X,Y,"y+",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # yelloq
		halo7=halos7.next()
		X,Y=(float(halo7[0]),float(halo7[1])) #Circle
		p.plot(X,Y,color="#FF9977",marker="+",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # orange
		correction=corrections.next()
		X,Y=float(correction[1]),float(correction[2])

		p.plot(X,Y,color="r",marker="+",ms=MARKERSIZE,markeredgewidth=MARKERTHICKNESS,alpha=ALHPA) # red
		if float(halo[6])!=0:

			X,Y=ellipse(200,200,3.1416/2,float(halo[6]),float(halo[7]))
			p.plot(X,Y,"g.-",ms=2) # green ellipse
			X,Y=ellipse(200,200,3.1416/2,float(halo2[3]),float(halo2[4]))
			p.plot(X,Y,"r.-",ms=2) # red ellhalopse
			X,Y=ellipse(200,200,3.1416/2,float(halo3[3]),float(halo3[4]))
			p.plot(X,Y,"m.-",ms=2) # magenta ellhalopse

		if float(halo[8])!=0:
			X,Y=ellipse(200,200,3.1416/2,float(halo[8]),float(halo[9]))
			p.plot(X,Y,"g.-",ms=2) # green ellipse
			X,Y=ellipse(200,200,3.1416/2,float(halo2[5]),float(halo2[6]))
			p.plot(X,Y,"r.-",ms=2) # red 
			X,Y=ellipse(200,200,3.1416/2,float(halo3[5]),float(halo3[6]))
			p.plot(X,Y,"m.-",ms=2) # magenta 

		
		#break

		p.grid(True)
		#p.legend( ('label1', 'label2', 'label3') )

		#ax = p.gca()
		#ax.yaxis.set_visible(False)
		#ax.xaxis.set_visible(False)
		#p.tight_layout(pad=0)
		p.savefig(pp, format='pdf')
		p.clf()
	pp.close()

if __name__ == '__main__':
	test()