#Kaggle Dark World Competition

Team paheli

1. `bias/` contains script to find bias.
2. `Circles/` contain circles analytical method.
3. `docs/` contains misc documents 
4. `ellipse/` contains heuristic algorithm called ellipse. `ellipse/oldellipse/` contains old versions of this algorithm.
5. `ensemble/` contains scripts which create ensemble using various methods.
6. `Grided_Signal/` contains Grid benchmark.
7. `learningModels/` contain learning algorithms.
8. `Lens/` contains methods to improve on the lenstool.
9. `lenstool/` contains quick and dirty lenstool prediction scripts.
10. `logs/` contain various logs/results.
11. `maximumLikelihood/` contains maximum likelihood benchmark.
12. `misc/` contains miscellenious files and scripts.
13. `plots/` contain various plots.
14. `processed/` contains processed data.
15. `Raw/` contains raw data.
16. `SuperHalo/` Contains script to find superhalo.
17. `tempimages/` contain processed data in from of images.
18. `tools/` contain misc tools.

Lenstool
--------
preprocesslt.py preprocesses data for lenstool

run*.py runs lenstool one by one for each sky and stores results in csv file

Visualizations
-------------

I have visualized predictions from all algorithms in plots/*.pdf

NEW LEGEND:
____________
I have changed color scheme in training2halos.pdf

* `cyan` ground truth
* `navy blue` normal lenstool results
* `gray` normal lenstool results assuming only 1 halo
* `greyish blue` antihalo lenstool result 1 halo
* `pink cross` centroids (new lens)
* `light red` old points algo
* `dark red` new points algorithm with antihalo
* `green` gridded signal
* `light orange` normal maxlikelihood
* `dark orange` antihalo maxlikelihood
* `light purple` normal random forest
* `dark purple` antihalo random forest

OLD LEGEND:
-------

* `cyan` ground truth
* `magenta` rf
* `green` Gridded benchmark
* `blue` lenstool
* `red` heuristic ellipse 
* `yellow` Maximum lilehood benchmark
* `pink` Circle analytical algorithm

LOGS
------

`logs/training60results` contains darkmetrics results for all algorithms for Trainingset[60:99]


For more info read comments in the scripts.

