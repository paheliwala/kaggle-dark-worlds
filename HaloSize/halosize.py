import pandas
import cmath
import math
import numpy

halos = pandas.read_csv( "../Raw/Training_halos.csv" )

def distance( ):
    pass

def gal_heading( v ):
    a = math.atan2( v['e2'],v['e1'] )/2 + math.pi/2;
    return a

def estimate_size( sky_name ):
    filename = "../Raw/Train_Skies/Training_%s.csv" % sky_name
    h1 = halos[halos.SkyId == sky_name ]
    halo_z = float( h1.halo_x1 ) + 1j*float( h1.halo_y1 )
    sky = pandas.read_csv( filename )
    sky['z1'] = sky.x+1j*sky.y - halo_z
    sky['l1'] = sky['z1'].map(lambda x: abs(x))
    sky['a1'] = sky['z1'].map(lambda x: cmath.phase(x)) # Force angle
    sky_sub = sky[sky.l1 < 700]
    sky_sub['ga'] = sky_sub.apply(gal_heading,1)
    sky_sub['sa'] = numpy.abs( numpy.sin( sky_sub['ga'] - sky_sub['a1'] ) )
    return ( sky_sub['sa'].mean() )

#print(halos)
for i in xrange(1,301) :
    sky_name = "Sky%d" % i
    print i, estimate_size( sky_name )


