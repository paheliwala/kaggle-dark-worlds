library(ggplot2)
library(reshape2)

rm(list=objects())


get_sky <- function( sky, x, y ) {
    path <- sprintf( "../Raw/Train_Skies/Training_Sky%d.csv", sky )
    features <- read.table( file=path, header=TRUE, sep=',' )
    features$GalaxyID <- NULL
    features$x <- features$x - x
    features$y <- features$y - y

    features
}

halo_count <- read.table( 
    '../Raw/Training_halos.csv',
    header=TRUE, sep=',' )

supersky <- data.frame()
for ( i in 1:100 ) {
    print( i )
    xx <- halo_count[i,'halo_x1']
    yy <- halo_count[i,'halo_y1']
    supersky <- rbind( supersky, get_sky( i, xx, yy ) )
}


# Tell if a coordinate (v) is in range of
# coordinate (xref,yref)
in_range <- function( v, xref, yref, range) {
  dx <- v[1]-xref
  dy <- v[2]-yref
  dist <- sqrt( dx*dx + dy*dy )
  dist < range
}

# Angle of the Force Vector
force <- function(v,xref,yref) {
  dx <- v[1]-xref
  dy <- v[2]-yref
  atan2(dy,dx) + pi
}

# Deflection
aim <- function( v ) {
  angle <- v[1]
  force <- v[2]
  d <- angle - force
  if ( d < -0.25*pi ) {
    d <- d + 2*pi
  }
  if ( d > 0.25*pi ) {
    d <- d - pi
  }
  abs( d )
}

# Deflection angle of the galaxy
angle <- function(v) { atan2(v[1],v[2])/2 + pi/2 }

# Fitness of point xref, yref
fitness <- function( v ) {
  print(v)
  x <- v[1]
  y <- v[2]
  subs <- apply(X=supersky[,c('x','y')],MARGIN=1,FUN=in_range,xref=x,yref=y,range=1600)
  sub_sky <- supersky[ subs, ]
  sub_sky$theta  <- apply(X=sub_sky[,c('e2','e1')],MARGIN=1,FUN=angle)
  sub_sky$force  <- apply(X=sub_sky[,c('x','y')],MARGIN=1,FUN=force,xref=x,yref=y)
  sub_sky$aim    <- apply(X=sub_sky[,c('theta','force')],MARGIN=1,FUN=aim)
  m <- mean( sub_sky$aim )
  m
}


#supersky$force  <- apply(X=supersky[,c('x','y')],MARGIN=1,FUN=force,xref=0,yref=0)
#supersky$theta  <- apply(X=supersky[,c('e2','e1')],MARGIN=1,FUN=angle)
#supersky$aim    <- apply(X=supersky[,c('theta','force')],MARGIN=1,FUN=aim)

#p <- ggplot( supersky, aes(x=x,y=y,colour=aim) ) + geom_point()

z <- seq(-4000,4000,by=200)
x <- rep(z,each=length(z))
y <- rep(z,length(z))

peek <- data.frame(x,y)
peek$f_mean  <- apply(X=peek[,c('x','y')],MARGIN=1,FUN=fitness)
peek <- melt(peek,id.vars=c('x','y'))

p <- ggplot(peek,aes(x=x,y=y,fill=scale(value))) + 
  geom_tile() + scale_fill_gradient2(low='red',mid='black', high='green') +
  facet_wrap(~variable,nrow=2,ncol=2)


