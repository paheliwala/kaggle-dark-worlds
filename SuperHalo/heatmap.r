# vi: spell spl=en
#

rm(list=objects())

library(ggplot2)

features        <- read.table( 'super_halo.csv', header=TRUE, sep=',' )
features$f_mean <- scale( features$f_mean )

p <- ggplot( features, 
    aes( x=x, y=y, fill=f_mean ) ) +
    geom_tile() +
    scale_fill_gradientn( colours = rainbow(7) )

ggsave( plot=p, file='super_halo.pdf', width=10, height=10 )

