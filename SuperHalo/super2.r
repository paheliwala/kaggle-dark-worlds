library(ggplot2)
library(reshape2)

rm(list=objects())


get_sky <- function( sky, x, y ) {
    path <- sprintf( "../Raw/Train_Skies/Training_Sky%d.csv", sky )
    features <- read.table( file=path, header=TRUE, sep=',' )
#    features$GalaxyID <- NULL
    features$x <- features$x - x
    features$y <- features$y - y

    features
}

halo_count <- read.table( 
    '../Raw/Training_halos.csv',
    header=TRUE, sep=',' )

supersky <- data.frame()
for ( i in 1:100 ) {
    print( i )
    xx <- halo_count[i,'halo_x1']
    yy <- halo_count[i,'halo_y1']
    supersky <- rbind( supersky, get_sky( i, xx, yy ) )
}

write.table( supersky, 
    file='super_sky.csv', col.names = TRUE, sep=',',
    row.names = FALSE, quote=FALSE )

