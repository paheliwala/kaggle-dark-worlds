library(reshape2)
library(ggplot2)

supersky  <- read.table( 'Feat.csv', sep=',', header=TRUE )
normalsky <- read.table( 'NormalSky.csv', sep=',', header=TRUE )

normalsky$f_org  <- scale( normalsky$f_mean )
normalsky$f_mean <- scale(normalsky$f_mean) - 0.5*scale( supersky$f_mean )

normalsky$x <- normalsky$x + 1500
normalsky$y <- normalsky$y + 3100

normalsky <- normalsky[normalsky$x>0,]
normalsky <- normalsky[normalsky$y>0,]
normalsky <- normalsky[normalsky$x<4000,]
normalsky <- normalsky[normalsky$y<4000,]

#normalsky$f_mean <- scale( normalsky$f_mean )

halos <- read.table( 
    '../Raw/Training_halos.csv', header=TRUE, sep=',' )
this_halos <- halos[ halos$SkyId == 'Sky154',]

normalsky$inter_gal <- NULL
normalsky$gal_count <- NULL
normalsky <- melt( normalsky, id.vars=c('x','y') )
normalsky$value <- scale( normalsky$value )
p <- ggplot(normalsky,aes(x=x,y=y,fill=value))+geom_tile() +
    scale_fill_gradient2(low='red',mid='black', high='green') +
    facet_wrap(~variable,nrow=2) +
    geom_point( 
        data=this_halos,
        aes( x=halo_x1, y=halo_y1 ), 
        fill='black', colour='white', size=10 ) +
    geom_point( 
        data=this_halos,
        aes( x=halo_x2, y=halo_y2 ), 
        fill='black', colour='white', size=10 )



