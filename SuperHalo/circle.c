/* vi: spell spl=en 
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>


/* To hold the data from Sky files */
struct Galaxy {
    double x;
    double y;
    double e1;
    double e2;
    struct Galaxy* next;        /* Linked list for the whole set */
    struct Galaxy* subset_next; /* Linked list for a subset */
};

void show( struct Galaxy* g )
{
    for ( ; g; g = g->next ) {
        printf("%f %f %f %f\n", g->x, g->y, g->e1, g->e2 );
    }
}

void show_subset( struct Galaxy* g )
{
    for ( ; g; g = g->subset_next ) {
        printf("%f %f %f %f\n", g->x, g->y, g->e1, g->e2 );
    }
}


double distance( double x1, double y1, double x2, double y2 )
{
    double dx = x1 - x2;
    double dy = y1 - y2;
    return ( sqrt( dx*dx + dy*dy ) );
}

struct Galaxy* near( struct Galaxy *g, double xref, double yref, double r )
{
    struct Galaxy* subset = NULL;
    for ( ; g; g = g->next ) {
        if ( distance( xref, yref, g->x, g->y ) < r ) {
            g->subset_next = subset;
            subset = g;
        }
    }
    return subset;
}

struct Galaxy* read_galaxies( char* filename )
{
    struct Galaxy* r = NULL;
    FILE *in;
    char buffer[1024];

    in = fopen( filename, "r" );
    if ( in ) {
        char* s;
        // Skip header
        s = fgets( buffer, 1024, in );
        while ( fgets( buffer, 1024, in ) ) {
            // printf("%s", buffer);
            struct Galaxy* g = malloc( sizeof( struct Galaxy ) );
            // Name of galaxy...
            s = strtok( buffer, "," );
            s = strtok( NULL, "," );
            g->x = atof(s);
            s = strtok( NULL, "," );
            g->y = atof(s);
            s = strtok( NULL, "," );
            g->e1 = atof(s);
            s = strtok( NULL, "," );
            g->e2 = atof(s);
            g->next = r;
            g->subset_next = NULL;
            r = g;
        }
    }
    printf( "\n" );
    return r;
}


// Just one feature for now.
struct FeatureSet {
    double mean_force_dev;
    double gal_count;
    double inter_gal;
};

struct FeatureSet compute_features( 
        struct Galaxy* g, double xref, double yref ) 
{
    struct Galaxy* i_g = NULL;
    struct Galaxy* j_g = NULL;
    struct FeatureSet fs;
    double mean_d = 0.0;
    int n = 0;
    for ( i_g = g ; 
          i_g; 
          i_g = i_g->subset_next ) {
        double force_angle, galaxy_angle, dx, dy, d;
        dx = xref - i_g->x;
        dy = yref - i_g->y;
        force_angle = atan2( dy, dx );
        galaxy_angle = atan2( i_g->e2, i_g->e1 )/2 + M_PI/2;

        d = force_angle - galaxy_angle;
        if ( d < -0.25*M_PI ) {
            d = d + 2*M_PI;
        }
        if ( d > 0.25*M_PI ) {
            d = d - M_PI;
        }
        d = fabs( d );

        mean_d += d;
        n++;
    }
    fs.mean_force_dev = mean_d/n;
    fs.gal_count = n;

    return fs;
}


double* compute_heat_map( 
        FILE* out, struct Galaxy* galaxies,
        int step, int xy_max, int m 
) {
    double xref;
    double yref;
    double* heat_map = NULL;

    int i,j;
    int n = 0;

    fprintf( out, "%s\n", "x,y,f_mean,gal_count,inter_gal" );
    for ( xref = -4000, i = 0; 
            xref <= xy_max; 
            xref = xref + step, ++i ) {
        for ( yref = -4000, j = 0;
                yref <= xy_max; 
                yref = yref + step, j++ ) {
            struct FeatureSet fs;

            fs = compute_features( 
                    near( galaxies, xref, yref, 1600 ),
                    xref, yref );

            fprintf( out, "%f,%f,%f,%f,%f\n", 
                    xref, yref, 
                    fs.mean_force_dev,
                    fs.gal_count,
                    fs.inter_gal );
            ++n;
        }
    }
    printf("Processed %d points\n", n );

    return NULL;
}

int main( int argc, char** argv )
{
    struct Galaxy* galaxies;
    int result = EXIT_FAILURE;

    if ( argc < 3 ) {
        fprintf( stderr, "Too few parameters" );
    } else { // All is OK
        FILE *out;
        galaxies = read_galaxies( argv[1] ); 
        out = fopen( argv[2], "w" );
        if ( out ) {
            double* heat_map = NULL;
            int step   = 50;
            int xy_max = 4000;
            int m = xy_max/step + 1;

            heat_map = compute_heat_map( out, galaxies,
                   step, xy_max, m );
            fclose( out );

            out = fopen( "circle_par.csv", "w" );
            if ( out ) {
                fprintf( out, "%s\n", "xy_max,step,m" );
                fprintf( out, "%d,%d,%d\n", xy_max, step, m );
                fclose( out );
                result = EXIT_SUCCESS;
            } else {
                fprintf( stderr, "Can't open circle_par\n" );
            }
        } else {
            fprintf( stderr, "Can't open %s\n", argv[2] );
        }
    }
    return result;
}

