import csv
import subprocess
import math
ans=[]
#lenses=csv.reader(open("../lenstool/lenstooltraining2halo.csv","rb"))
lenses=csv.reader(open("../misc/lenstool.benchmark2halos.csv","rb"))
lenses.next()
points=csv.reader(open("../ellipse/testpoints2halos.formatted.csv","rb"))
ans.append(points.next())

for lens in lenses:
	point=points.next()
	if math.sqrt((float(lens[1])-float(lens[3]))**2+(float(lens[2])-float(lens[4]))**2)<400:
		if math.sqrt((float(point[1])-float(point[3]))**2+(float(point[2])-float(point[4]))**2)>400:
			ans.append(point)
			continue
	ans.append(lens)
#print ans
f=open("test2halopoilens.csv","wb")
writer=csv.writer(f)
writer.writerows(ans)
f.close()
subprocess.call(["python", "../tools/DarkWorldsMetric.py", "../ensemble/2halopoilens.csv", "../tools/2Training_halos.csv"]) 
