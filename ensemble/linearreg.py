from sklearn.linear_model import LinearRegression
import numpy as np

mod1=np.genfromtxt("../Circles/predict60.csv", delimiter=",",skip_header=1,usecols=(1,2))
mod2=np.genfromtxt("../lenstool/lenstooltraining60.csv", delimiter=",",skip_header=1,usecols=(1,2))
mod3=np.genfromtxt("../learningModels/rfpredstraining60.csv", delimiter=",",skip_header=1,usecols=(1,2))
mod4=np.genfromtxt("../Gridded_Signal/Gridded_Signal_benchmark60.csv", delimiter=",",skip_header=1,usecols=(1,2))
mod5=np.genfromtxt("../maximumLikelihood/Maximum_likelihood_Train60.csv", delimiter=",",skip_header=1,usecols=(1,2))
y=np.genfromtxt("../tools/2Training_halos.csv", delimiter=",",skip_header=1,usecols=(4,5))

print len(mod1),len(mod2),len(mod3),len(mod4),len(mod5),len(y)
print np.array(mod1[:20,0]).T,mod2[:20,0]
X_train=np.array(np.vstack((mod1[:20,0],mod2[:20,0],mod3[:20,0],mod4[:20,0],mod5[:20,0])))
print np.shape(X_train)
y_train=y[:20,0]
y_test=y[20:,0]
clf=LinearRegression().fit(X_train.T,y[:20,0])
X_test=np.array(np.vstack((mod1[20:,0],mod2[20:,0],mod3[20:,0],mod4[20:,0],mod5[20:,0])))
print np.shape(X_test)
preds=clf.predict(X_test.T)
print preds
print y_test
print clf.score(X_test.T,y_test)

print np.array(mod1[:20,1]).T,mod2[:20,1]
X_train=np.array(np.vstack((mod1[:20,1],mod2[:20,1],mod3[:20,1],mod4[:20,1],mod5[:20,1])))
print np.shape(X_train)
y_train=y[:20,1]
y_test=y[20:,1]
clf=LinearRegression().fit(X_train.T,y[:20,1])
X_test=np.array(np.vstack((mod1[20:,1],mod2[20:,1],mod3[20:,1],mod4[20:,1],mod5[20:,1])))
preds=clf.predict(X_test.T)
print preds
print y_test